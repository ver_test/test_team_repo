﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using System.Security.Permissions;
using DevExpress.Utils;
using DevExpress.XtraEditors;
using DevExpress.XtraEditors.Controls;

namespace RTLComponent_Test
{
    public partial class XtraForm1_EditorC : DevExpress.XtraEditors.XtraForm
    {
        const int WS_EX_LAYOUTRTL = 0x400000;
        const int WS_EX_RTLREADING = 0x2000;      

        public XtraForm1_EditorC()
        {
            InitializeComponent();

            //filling components
            string[] values = { "This", "Item", "Should", "Work" };
            foreach (string val in values)
            {               
                rtL_CheckedComboBoxEdit1.Properties.Items.Add(val, CheckState.Unchecked, true);
                rtL_ComboBoxEdit1.Properties.Items.Add(val);
                rtL_ComboBoxEdit2.Properties.Items.Add(val);
                rtL_ImageComboBoxEdit1.Properties.Items.Add(new ImageComboBoxItem(val));
                rtL_ListBoxControl1.Items.AddRange(values);
            }                                      
        }


        //protected override CreateParams CreateParams
        //{
        //    get
        //    {
        //        new SecurityPermission(SecurityPermissionFlag.UnmanagedCode).Demand();

        //        CreateParams CP;
        //        CP = base.CreateParams;
        //        CP.ExStyle = CP.ExStyle | WS_EX_LAYOUTRTL | WS_EX_RTLREADING;
        //        return CP;
        //    }
        //}

       


    }

    public class RTL_ButtonEdit : DevExpress.XtraEditors.ButtonEdit
    {
        const int WS_EX_LAYOUTRTL = 0x400000;
        const int WS_EX_RTLREADING = 0x2000;

        private bool _mirrored = false;

        [Description("Change to the right-to-left layout."), DefaultValue(false), Localizable(true), Category("Appearance"), Browsable(true)]
        public bool Mirrored
        {
            get
            {
                return _mirrored;
            }
            set
            {
                if (_mirrored != value)
                {
                    _mirrored = value;
                    this.Refresh();
                    base.OnRightToLeftChanged(EventArgs.Empty);
                }
            }
        }

        protected override CreateParams CreateParams
        {
            get
            {
                if (this.Mirrored)
                {
                    CreateParams CP;
                    CP = base.CreateParams;
                    CP.ExStyle = CP.ExStyle | WS_EX_LAYOUTRTL | WS_EX_RTLREADING;
                    return CP;
                }
                else
                {
                    return base.CreateParams;
                }
            }
        }
    }

    public class RTL_CalcEdit : DevExpress.XtraEditors.CalcEdit
    {
        const int WS_EX_LAYOUTRTL = 0x400000;
        const int WS_EX_RTLREADING = 0x2000;

        private bool _mirrored = false;

        [Description("Change to the right-to-left layout."), DefaultValue(false), Localizable(true), Category("Appearance"), Browsable(true)]
        public bool Mirrored
        {
            get
            {
                return _mirrored;
            }
            set
            {
                if (_mirrored != value)
                {
                    _mirrored = value;
                    this.Refresh();
                    base.OnRightToLeftChanged(EventArgs.Empty);
                }
            }
        }

        protected override CreateParams CreateParams
        {
            get
            {
                if (this.Mirrored)
                {
                    CreateParams CP;
                    CP = base.CreateParams;
                    CP.ExStyle = CP.ExStyle | WS_EX_LAYOUTRTL | WS_EX_RTLREADING;
                    return CP;
                }
                else
                {
                    return base.CreateParams;
                }
            }
        }
    }

    public class RTL_CheckButton : DevExpress.XtraEditors.CheckButton
    {
        const int WS_EX_LAYOUTRTL = 0x400000;
        const int WS_EX_RTLREADING = 0x2000;

        private bool _mirrored = false;

        [Description("Change to the right-to-left layout."), DefaultValue(false), Localizable(true), Category("Appearance"), Browsable(true)]
        public bool Mirrored
        {
            get
            {
                return _mirrored;
            }
            set
            {
                if (_mirrored != value)
                {
                    _mirrored = value;
                    this.Refresh();
                    base.OnRightToLeftChanged(EventArgs.Empty);
                }
            }
        }

        protected override CreateParams CreateParams
        {
            get
            {
                if (this.Mirrored)
                {
                    CreateParams CP;
                    CP = base.CreateParams;
                    CP.ExStyle = CP.ExStyle | WS_EX_LAYOUTRTL | WS_EX_RTLREADING;
                    return CP;
                }
                else
                {
                    return base.CreateParams;
                }
            }
        }
    }

    public class RTL_CheckedComboBoxEdit : DevExpress.XtraEditors.CheckedComboBoxEdit
    {
        const int WS_EX_LAYOUTRTL = 0x400000;
        const int WS_EX_RTLREADING = 0x2000;

        private bool _mirrored = false;

        [Description("Change to the right-to-left layout."), DefaultValue(false), Localizable(true), Category("Appearance"), Browsable(true)]
        public bool Mirrored
        {
            get
            {
                return _mirrored;
            }
            set
            {
                if (_mirrored != value)
                {
                    _mirrored = value;
                    this.Refresh();
                    base.OnRightToLeftChanged(EventArgs.Empty);
                }
            }
        }

        protected override CreateParams CreateParams
        {
            get
            {
                if (this.Mirrored)
                {
                    CreateParams CP;
                    CP = base.CreateParams;
                    CP.ExStyle = CP.ExStyle | WS_EX_LAYOUTRTL | WS_EX_RTLREADING;
                    return CP;
                }
                else
                {
                    return base.CreateParams;
                }
            }
        }      
    }

    public class RTL_CheckEdit : DevExpress.XtraEditors.CheckEdit
    {
        const int WS_EX_LAYOUTRTL = 0x400000;
        const int WS_EX_RTLREADING = 0x2000;

        private bool _mirrored = false;

        [Description("Change to the right-to-left layout."), DefaultValue(false), Localizable(true), Category("Appearance"), Browsable(true)]
        public bool Mirrored
        {
            get
            {
                return _mirrored;
            }
            set
            {
                if (_mirrored != value)
                {
                    _mirrored = value;
                    this.Refresh();
                    base.OnRightToLeftChanged(EventArgs.Empty);
                }
            }
        }

        protected override CreateParams CreateParams
        {
            get
            {
                if (this.Mirrored)
                {
                    CreateParams CP;
                    CP = base.CreateParams;
                    CP.ExStyle = CP.ExStyle | WS_EX_LAYOUTRTL | WS_EX_RTLREADING;
                    return CP;
                }
                else
                {
                    return base.CreateParams;
                }
            }
        }
    }

    public class RTL_ColorEdit : DevExpress.XtraEditors.ColorEdit
    {
        const int WS_EX_LAYOUTRTL = 0x400000;
        const int WS_EX_RTLREADING = 0x2000;

        private bool _mirrored = false;

        [Description("Change to the right-to-left layout."), DefaultValue(false), Localizable(true), Category("Appearance"), Browsable(true)]
        public bool Mirrored
        {
            get
            {
                return _mirrored;
            }
            set
            {
                if (_mirrored != value)
                {
                    _mirrored = value;
                    this.Refresh();
                    base.OnRightToLeftChanged(EventArgs.Empty);
                }
            }
        }

        protected override CreateParams CreateParams
        {
            get
            {
                if (this.Mirrored)
                {
                    CreateParams CP;
                    CP = base.CreateParams;
                    CP.ExStyle = CP.ExStyle | WS_EX_LAYOUTRTL | WS_EX_RTLREADING;
                    return CP;
                }
                else
                {
                    return base.CreateParams;
                }
            }
        }
    }

    public class RTL_ComboBoxEdit : DevExpress.XtraEditors.ComboBoxEdit
    {
        const int WS_EX_LAYOUTRTL = 0x400000;
        const int WS_EX_RTLREADING = 0x2000;

        private bool _mirrored = false;

        [Description("Change to the right-to-left layout."), DefaultValue(false), Localizable(true), Category("Appearance"), Browsable(true)]
        public bool Mirrored
        {
            get
            {
                return _mirrored;
            }
            set
            {
                if (_mirrored != value)
                {
                    _mirrored = value;
                    this.Refresh();
                    base.OnRightToLeftChanged(EventArgs.Empty);
                }
            }
        }

        protected override CreateParams CreateParams
        {
            get
            {
                if (this.Mirrored)
                {
                    CreateParams CP;
                    CP = base.CreateParams;
                    CP.ExStyle = CP.ExStyle | WS_EX_LAYOUTRTL | WS_EX_RTLREADING;
                    return CP;
                }
                else
                {
                    return base.CreateParams;
                }
            }
        }
    }

    public class RTL_DataNavigator : DevExpress.XtraEditors.DataNavigator
    {
        const int WS_EX_LAYOUTRTL = 0x400000;
        const int WS_EX_RTLREADING = 0x2000;

        private bool _mirrored = false;

        [Description("Change to the right-to-left layout."), DefaultValue(false), Localizable(true), Category("Appearance"), Browsable(true)]
        public bool Mirrored
        {
            get
            {
                return _mirrored;
            }
            set
            {
                if (_mirrored != value)
                {
                    _mirrored = value;
                    this.Refresh();
                    base.OnRightToLeftChanged(EventArgs.Empty);
                }
            }
        }

        protected override CreateParams CreateParams
        {
            get
            {
                if (this.Mirrored)
                {
                    CreateParams CP;
                    CP = base.CreateParams;
                    CP.ExStyle = CP.ExStyle | WS_EX_LAYOUTRTL | WS_EX_RTLREADING;
                    return CP;
                }
                else
                {
                    return base.CreateParams;
                }
            }
        }
    }

    public class RTL_DateEdit : DevExpress.XtraEditors.DateEdit
    {
        const int WS_EX_LAYOUTRTL = 0x400000;
        const int WS_EX_RTLREADING = 0x2000;

        private bool _mirrored = false;

        [Description("Change to the right-to-left layout."), DefaultValue(false), Localizable(true), Category("Appearance"), Browsable(true)]
        public bool Mirrored
        {
            get
            {
                return _mirrored;
            }
            set
            {
                if (_mirrored != value)
                {
                    _mirrored = value;
                    this.Refresh();
                    base.OnRightToLeftChanged(EventArgs.Empty);
                }
            }
        }

        protected override CreateParams CreateParams
        {
            get
            {
                if (this.Mirrored)
                {
                    CreateParams CP;
                    CP = base.CreateParams;
                    CP.ExStyle = CP.ExStyle | WS_EX_LAYOUTRTL | WS_EX_RTLREADING;
                    return CP;
                }
                else
                {
                    return base.CreateParams;
                }
            }
        }
    }

    public class RTL_DropDownButton : DevExpress.XtraEditors.DropDownButton
    {
        const int WS_EX_LAYOUTRTL = 0x400000;
        const int WS_EX_RTLREADING = 0x2000;

        private bool _mirrored = false;

        [Description("Change to the right-to-left layout."), DefaultValue(false), Localizable(true), Category("Appearance"), Browsable(true)]
        public bool Mirrored
        {
            get
            {
                return _mirrored;
            }
            set
            {
                if (_mirrored != value)
                {
                    _mirrored = value;
                    this.Refresh();
                    base.OnRightToLeftChanged(EventArgs.Empty);
                }
            }
        }

        protected override CreateParams CreateParams
        {
            get
            {
                if (this.Mirrored)
                {
                    CreateParams CP;
                    CP = base.CreateParams;
                    CP.ExStyle = CP.ExStyle | WS_EX_LAYOUTRTL | WS_EX_RTLREADING;
                    return CP;
                }
                else
                {
                    return base.CreateParams;
                }
            }
        }
    }

    public class RTL_FilterControl : DevExpress.XtraEditors.FilterControl
    {
        const int WS_EX_LAYOUTRTL = 0x400000;
        const int WS_EX_RTLREADING = 0x2000;

        private bool _mirrored = false;

        [Description("Change to the right-to-left layout."), DefaultValue(false), Localizable(true), Category("Appearance"), Browsable(true)]
        public bool Mirrored
        {
            get
            {
                return _mirrored;
            }
            set
            {
                if (_mirrored != value)
                {
                    _mirrored = value;
                    this.Refresh();
                    base.OnRightToLeftChanged(EventArgs.Empty);
                }
            }
        }

        protected override CreateParams CreateParams
        {
            get
            {
                if (this.Mirrored)
                {
                    CreateParams CP;
                    CP = base.CreateParams;
                    CP.ExStyle = CP.ExStyle | WS_EX_LAYOUTRTL | WS_EX_RTLREADING;
                    return CP;
                }
                else
                {
                    return base.CreateParams;
                }
            }
        }
    }

    public class RTL_FontEdit : DevExpress.XtraEditors.FontEdit
    {
        const int WS_EX_LAYOUTRTL = 0x400000;
        const int WS_EX_RTLREADING = 0x2000;

        private bool _mirrored = false;

        [Description("Change to the right-to-left layout."), DefaultValue(false), Localizable(true), Category("Appearance"), Browsable(true)]
        public bool Mirrored
        {
            get
            {
                return _mirrored;
            }
            set
            {
                if (_mirrored != value)
                {
                    _mirrored = value;
                    this.Refresh();
                    base.OnRightToLeftChanged(EventArgs.Empty);
                }
            }
        }

        protected override CreateParams CreateParams
        {
            get
            {
                if (this.Mirrored)
                {
                    CreateParams CP;
                    CP = base.CreateParams;
                    CP.ExStyle = CP.ExStyle | WS_EX_LAYOUTRTL | WS_EX_RTLREADING;
                    return CP;
                }
                else
                {
                    return base.CreateParams;
                }
            }
        }
    }

    public class RTL_GroupControl : DevExpress.XtraEditors.GroupControl
    {
        const int WS_EX_LAYOUTRTL = 0x400000;
        const int WS_EX_RTLREADING = 0x2000;

        private bool _mirrored = false;

        [Description("Change to the right-to-left layout."), DefaultValue(false), Localizable(true), Category("Appearance"), Browsable(true)]
        public bool Mirrored
        {
            get
            {
                return _mirrored;
            }
            set
            {
                if (_mirrored != value)
                {
                    _mirrored = value;
                    this.Refresh();
                    base.OnRightToLeftChanged(EventArgs.Empty);
                }
            }
        }

        protected override CreateParams CreateParams
        {
            get
            {
                if (this.Mirrored)
                {
                    CreateParams CP;
                    CP = base.CreateParams;
                    CP.ExStyle = CP.ExStyle | WS_EX_LAYOUTRTL | WS_EX_RTLREADING;
                    return CP;
                }
                else
                {
                    return base.CreateParams;
                }
            }
        }
    }

    public class RTL_HyperLinkEdit : DevExpress.XtraEditors.HyperLinkEdit
    {
        const int WS_EX_LAYOUTRTL = 0x400000;
        const int WS_EX_RTLREADING = 0x2000;

        private bool _mirrored = false;

        [Description("Change to the right-to-left layout."), DefaultValue(false), Localizable(true), Category("Appearance"), Browsable(true)]
        public bool Mirrored
        {
            get
            {
                return _mirrored;
            }
            set
            {
                if (_mirrored != value)
                {
                    _mirrored = value;
                    this.Refresh();
                    base.OnRightToLeftChanged(EventArgs.Empty);
                }
            }
        }

        protected override CreateParams CreateParams
        {
            get
            {
                if (this.Mirrored)
                {
                    CreateParams CP;
                    CP = base.CreateParams;
                    CP.ExStyle = CP.ExStyle | WS_EX_LAYOUTRTL | WS_EX_RTLREADING;
                    return CP;
                }
                else
                {
                    return base.CreateParams;
                }
            }
        }
    }

    public class RTL_ImageComboBoxEdit: DevExpress.XtraEditors.ImageComboBoxEdit
    {
        const int WS_EX_LAYOUTRTL = 0x400000;
        const int WS_EX_RTLREADING = 0x2000;

        private bool _mirrored = false;

        [Description("Change to the right-to-left layout."), DefaultValue(false), Localizable(true), Category("Appearance"), Browsable(true)]
        public bool Mirrored
        {
            get
            {
                return _mirrored;
            }
            set
            {
                if (_mirrored != value)
                {
                    _mirrored = value;
                    this.Refresh();
                    base.OnRightToLeftChanged(EventArgs.Empty);
                }
            }
        }

        protected override CreateParams CreateParams
        {
            get
            {
                if (this.Mirrored)
                {
                    CreateParams CP;
                    CP = base.CreateParams;
                    CP.ExStyle = CP.ExStyle | WS_EX_LAYOUTRTL | WS_EX_RTLREADING;
                    return CP;
                }
                else
                {
                    return base.CreateParams;
                }
            }
        }
    }

    public class RTL_ImageEdit : DevExpress.XtraEditors.ImageEdit
    {
        const int WS_EX_LAYOUTRTL = 0x400000;
        const int WS_EX_RTLREADING = 0x2000;

        private bool _mirrored = false;

        [Description("Change to the right-to-left layout."), DefaultValue(false), Localizable(true), Category("Appearance"), Browsable(true)]
        public bool Mirrored
        {
            get
            {
                return _mirrored;
            }
            set
            {
                if (_mirrored != value)
                {
                    _mirrored = value;
                    this.Refresh();
                    base.OnRightToLeftChanged(EventArgs.Empty);
                }
            }
        }

        protected override CreateParams CreateParams
        {
            get
            {
                if (this.Mirrored)
                {
                    CreateParams CP;
                    CP = base.CreateParams;
                    CP.ExStyle = CP.ExStyle | WS_EX_LAYOUTRTL | WS_EX_RTLREADING;
                    return CP;
                }
                else
                {
                    return base.CreateParams;
                }
            }
        }
    }

    public class RTL_LabelControl : DevExpress.XtraEditors.LabelControl
    {
        const int WS_EX_LAYOUTRTL = 0x400000;
        const int WS_EX_RTLREADING = 0x2000;

        private bool _mirrored = false;

        [Description("Change to the right-to-left layout."), DefaultValue(false), Localizable(true), Category("Appearance"), Browsable(true)]
        public bool Mirrored
        {
            get
            {
                return _mirrored;
            }
            set
            {
                if (_mirrored != value)
                {
                    _mirrored = value;
                    this.Refresh();
                    base.OnRightToLeftChanged(EventArgs.Empty);
                }
            }
        }

        protected override CreateParams CreateParams
        {
            get
            {
                if (this.Mirrored)
                {
                    CreateParams CP;
                    CP = base.CreateParams;
                    CP.ExStyle = CP.ExStyle | WS_EX_LAYOUTRTL | WS_EX_RTLREADING;
                    return CP;
                }
                else
                {
                    return base.CreateParams;
                }
            }
        }
    }

    public class RTL_ListBoxControl : DevExpress.XtraEditors.ListBoxControl
    {
        const int WS_EX_LAYOUTRTL = 0x400000;
        const int WS_EX_RTLREADING = 0x2000;

        private bool _mirrored = false;

        [Description("Change to the right-to-left layout."), DefaultValue(false), Localizable(true), Category("Appearance"), Browsable(true)]
        public bool Mirrored
        {
            get
            {
                return _mirrored;
            }
            set
            {
                if (_mirrored != value)
                {
                    _mirrored = value;
                    this.Refresh();
                    base.OnRightToLeftChanged(EventArgs.Empty);
                }
            }
        }

        protected override CreateParams CreateParams
        {
            get
            {
                if (this.Mirrored)
                {
                    CreateParams CP;
                    CP = base.CreateParams;
                    CP.ExStyle = CP.ExStyle | WS_EX_LAYOUTRTL | WS_EX_RTLREADING;
                    return CP;
                }
                else
                {
                    return base.CreateParams;
                }
            }
        }
    }

    public class RTL_LookUpEdit : DevExpress.XtraEditors.LookUpEdit
    {
        const int WS_EX_LAYOUTRTL = 0x400000;
        const int WS_EX_RTLREADING = 0x2000;

        private bool _mirrored = false;

        [Description("Change to the right-to-left layout."), DefaultValue(false), Localizable(true), Category("Appearance"), Browsable(true)]
        public bool Mirrored
        {
            get
            {
                return _mirrored;
            }
            set
            {
                if (_mirrored != value)
                {
                    _mirrored = value;
                    this.Refresh();
                    base.OnRightToLeftChanged(EventArgs.Empty);
                }
            }
        }

        protected override CreateParams CreateParams
        {
            get
            {
                if (this.Mirrored)
                {
                    CreateParams CP;
                    CP = base.CreateParams;
                    CP.ExStyle = CP.ExStyle | WS_EX_LAYOUTRTL | WS_EX_RTLREADING;
                    return CP;
                }
                else
                {
                    return base.CreateParams;
                }
            }
        }
    }

    public class RTL_MarqueeProgressBarControl : DevExpress.XtraEditors.MarqueeProgressBarControl
    {
        const int WS_EX_LAYOUTRTL = 0x400000;
        const int WS_EX_RTLREADING = 0x2000;

        private bool _mirrored = false;

        [Description("Change to the right-to-left layout."), DefaultValue(false), Localizable(true), Category("Appearance"), Browsable(true)]
        public bool Mirrored
        {
            get
            {
                return _mirrored;
            }
            set
            {
                if (_mirrored != value)
                {
                    _mirrored = value;
                    this.Refresh();
                    base.OnRightToLeftChanged(EventArgs.Empty);
                }
            }
        }

        protected override CreateParams CreateParams
        {
            get
            {
                if (this.Mirrored)
                {
                    CreateParams CP;
                    CP = base.CreateParams;
                    CP.ExStyle = CP.ExStyle | WS_EX_LAYOUTRTL | WS_EX_RTLREADING;
                    return CP;
                }
                else
                {
                    return base.CreateParams;
                }
            }
        }
    }

    public class RTL_MemoEdit : DevExpress.XtraEditors.MemoEdit
    {
        const int WS_EX_LAYOUTRTL = 0x400000;
        const int WS_EX_RTLREADING = 0x2000;

        private bool _mirrored = false;

        [Description("Change to the right-to-left layout."), DefaultValue(false), Localizable(true), Category("Appearance"), Browsable(true)]
        public bool Mirrored
        {
            get
            {
                return _mirrored;
            }
            set
            {
                if (_mirrored != value)
                {
                    _mirrored = value;
                    this.Refresh();
                    base.OnRightToLeftChanged(EventArgs.Empty);
                }
            }
        }

        protected override CreateParams CreateParams
        {
            get
            {
                if (this.Mirrored)
                {
                    CreateParams CP;
                    CP = base.CreateParams;
                    CP.ExStyle = CP.ExStyle | WS_EX_LAYOUTRTL | WS_EX_RTLREADING;
                    return CP;
                }
                else
                {
                    return base.CreateParams;
                }
            }
        }
    }

    public class RTL_MemoExEdit : DevExpress.XtraEditors.MemoExEdit
    {
        const int WS_EX_LAYOUTRTL = 0x400000;
        const int WS_EX_RTLREADING = 0x2000;

        private bool _mirrored = false;

        [Description("Change to the right-to-left layout."), DefaultValue(false), Localizable(true), Category("Appearance"), Browsable(true)]
        public bool Mirrored
        {
            get
            {
                return _mirrored;
            }
            set
            {
                if (_mirrored != value)
                {
                    _mirrored = value;
                    this.Refresh();
                    base.OnRightToLeftChanged(EventArgs.Empty);
                }
            }
        }

        protected override CreateParams CreateParams
        {
            get
            {
                if (this.Mirrored)
                {
                    CreateParams CP;
                    CP = base.CreateParams;
                    CP.ExStyle = CP.ExStyle | WS_EX_LAYOUTRTL | WS_EX_RTLREADING;
                    return CP;
                }
                else
                {
                    return base.CreateParams;
                }
            }
        }
    }

    public class RTL_MRUEdit : DevExpress.XtraEditors.MRUEdit
    {
        const int WS_EX_LAYOUTRTL = 0x400000;
        const int WS_EX_RTLREADING = 0x2000;

        private bool _mirrored = false;

        [Description("Change to the right-to-left layout."), DefaultValue(false), Localizable(true), Category("Appearance"), Browsable(true)]
        public bool Mirrored
        {
            get
            {
                return _mirrored;
            }
            set
            {
                if (_mirrored != value)
                {
                    _mirrored = value;
                    this.Refresh();
                    base.OnRightToLeftChanged(EventArgs.Empty);
                }
            }
        }

        protected override CreateParams CreateParams
        {
            get
            {
                if (this.Mirrored)
                {
                    CreateParams CP;
                    CP = base.CreateParams;
                    CP.ExStyle = CP.ExStyle | WS_EX_LAYOUTRTL | WS_EX_RTLREADING;
                    return CP;
                }
                else
                {
                    return base.CreateParams;
                }
            }
        }
    }

    public class RTL_PanelControl : DevExpress.XtraEditors.PanelControl
    {
        const int WS_EX_LAYOUTRTL = 0x400000;
        const int WS_EX_RTLREADING = 0x2000;

        private bool _mirrored = false;

        [Description("Change to the right-to-left layout."), DefaultValue(false), Localizable(true), Category("Appearance"), Browsable(true)]
        public bool Mirrored
        {
            get
            {
                return _mirrored;
            }
            set
            {
                if (_mirrored != value)
                {
                    _mirrored = value;
                    this.Refresh();
                    base.OnRightToLeftChanged(EventArgs.Empty);
                }
            }
        }

        protected override CreateParams CreateParams
        {
            get
            {
                if (this.Mirrored)
                {
                    CreateParams CP;
                    CP = base.CreateParams;
                    CP.ExStyle = CP.ExStyle | WS_EX_LAYOUTRTL | WS_EX_RTLREADING;
                    return CP;
                }
                else
                {
                    return base.CreateParams;
                }
            }
        }
    }

    public class RTL_PopupContainerEdit : DevExpress.XtraEditors.PopupContainerEdit
    {
        const int WS_EX_LAYOUTRTL = 0x400000;
        const int WS_EX_RTLREADING = 0x2000;

        private bool _mirrored = false;

        [Description("Change to the right-to-left layout."), DefaultValue(false), Localizable(true), Category("Appearance"), Browsable(true)]
        public bool Mirrored
        {
            get
            {
                return _mirrored;
            }
            set
            {
                if (_mirrored != value)
                {
                    _mirrored = value;
                    this.Refresh();
                    base.OnRightToLeftChanged(EventArgs.Empty);
                }
            }
        }

        protected override CreateParams CreateParams
        {
            get
            {
                if (this.Mirrored)
                {
                    CreateParams CP;
                    CP = base.CreateParams;
                    CP.ExStyle = CP.ExStyle | WS_EX_LAYOUTRTL | WS_EX_RTLREADING;
                    return CP;
                }
                else
                {
                    return base.CreateParams;
                }
            }
        }
    }
}