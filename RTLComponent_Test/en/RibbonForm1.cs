﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using System.Security.Permissions;
using DevExpress.XtraBars;

namespace RTLComponent_Test
{
    public partial class RibbonForm1 : DevExpress.XtraBars.Ribbon.RibbonForm
    {
        const int WS_EX_LAYOUTRTL = 0x400000;
        const int WS_EX_RTLREADING = 0x2000; 

        public RibbonForm1()
        {        
            InitializeComponent();           
        }

        protected override CreateParams CreateParams
        {
            get
            {
                new SecurityPermission(SecurityPermissionFlag.UnmanagedCode).Demand();

                CreateParams CP;
                CP = base.CreateParams;
                CP.ExStyle = CP.ExStyle | WS_EX_LAYOUTRTL | WS_EX_RTLREADING;
                return CP;
            }
        }
    }
}