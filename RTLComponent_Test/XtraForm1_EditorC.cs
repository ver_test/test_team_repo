﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using System.Windows.Forms.VisualStyles;
using System.Security.Permissions;
using DevExpress.Utils;
using DevExpress.XtraEditors;
using DevExpress.XtraEditors.Controls;
using DevExpress.XtraEditors.Popup;
using System.Resources;

namespace RTLComponent_Test
{
    public partial class XtraForm1_EditorC : DevExpress.XtraEditors.XtraForm
    {
        const int WS_EX_LAYOUTRTL = 0x400000;
        const int WS_EX_RTLREADING = 0x2000;
        const int WS_EX_RIGHT = 0x00001000;
        const int WS_EX_LEFTSCROLLBAR = 0x00004000;

        int Flg = 0;

        public XtraForm1_EditorC()
        {           
            InitializeComponent();
            //const string resxFile = "C:\\Users\\ptsipras\\Documents\\Visual Studio 2010\\Projects\\RTLComponent_Test\\RTLComponent_Test\\XtraForm1_EditorC.resx";
            //using (ResXResourceSet resxSet = new ResXResourceSet(resxFile))
            //{
                //filling components
                string[] values = { "This", "Item", "Should", "Work" };
                object[] pos = {10,11,12,13};
                foreach (string val in values)
                {
                    rtL_CheckedComboBoxEdit1.Properties.Items.Add(val, CheckState.Unchecked, true);
                    rtL_ComboBoxEdit1.Properties.Items.Add(val);
                    rtL_ComboBoxEdit2.Properties.Items.Add(val);
                    rtL_ImageComboBoxEdit1.Properties.Items.Add(new ImageComboBoxItem(val));
                    rtL_ListBoxControl1.Items.AddRange(values);                    
                }

                for (int i = 0; i < 4; i++)                 
                    rtL_RadioGroup1.Properties.Items.Add(new RadioGroupItem(pos[i],values[i]));               
           
                //progress bar
                rtL_ProgressBarControl1.Properties.Step = 1;
                rtL_ProgressBarControl1.Properties.PercentView = true;
                rtL_ProgressBarControl1.Properties.Maximum = 10;
                rtL_ProgressBarControl1.Properties.Minimum = 0;  
            //}
        }

        private void rtL_CheckButton1_CheckedChanged(object sender, EventArgs e)
        {
            //fill rtl_rtL_ProgressBarControl1
            rtL_ProgressBarControl1.PerformStep();
            rtL_ProgressBarControl1.Update();
            Flg++;
            if (Flg > 10)
            {
                rtL_ProgressBarControl1.Decrement(10);
                Flg = 0;
            }
        }

        protected override CreateParams CreateParams
        {
            get
            {           
                    CreateParams CP;
                    CP = base.CreateParams;
                    CP.ExStyle = CP.ExStyle | WS_EX_LAYOUTRTL | WS_EX_RTLREADING | WS_EX_RIGHT | WS_EX_LEFTSCROLLBAR;
                    return CP;
            }
                
        }
    }
    public class RTL_ButtonEdit : DevExpress.XtraEditors.ButtonEdit
    {
        const int WS_EX_LAYOUTRTL = 0x400000;
        const int WS_EX_RTLREADING = 0x2000;

        private bool _mirrored = false;

        [Description("Change to the right-to-left layout."), DefaultValue(false), Localizable(true), Category("Appearance"), Browsable(true)]
        public bool Mirrored
        {
            get
            {
                return _mirrored;
            }
            set
            {
                if (_mirrored != value)
                {
                    _mirrored = value;
                    this.Refresh();
                    base.OnRightToLeftChanged(EventArgs.Empty);
                }
            }
        }

        protected override CreateParams CreateParams
        {
            get
            {
                if (this.Mirrored)
                {
                    CreateParams CP;
                    CP = base.CreateParams;
                    CP.ExStyle = CP.ExStyle | WS_EX_LAYOUTRTL | WS_EX_RTLREADING;
                    return CP;
                }
                else
                {
                    return base.CreateParams;
                }
            }
        }
    }

    public class RTL_CalcEdit : DevExpress.XtraEditors.CalcEdit
    {
        const int WS_EX_LAYOUTRTL = 0x400000;
        const int WS_EX_RTLREADING = 0x2000;

        private bool _mirrored = false;

        [Description("Change to the right-to-left layout."), DefaultValue(false), Localizable(true), Category("Appearance"), Browsable(true)]
        public bool Mirrored
        {
            get
            {
                return _mirrored;
            }
            set
            {
                if (_mirrored != value)
                {
                    _mirrored = value;
                    this.Refresh();
                    base.OnRightToLeftChanged(EventArgs.Empty);
                }
            }
        }

        protected override CreateParams CreateParams
        {
            get
            {
                if (this.Mirrored)
                {
                    CreateParams CP;
                    CP = base.CreateParams;
                    CP.ExStyle = CP.ExStyle | WS_EX_LAYOUTRTL | WS_EX_RTLREADING;
                    return CP;
                }
                else
                {
                    return base.CreateParams;
                }
            }
        }
    }

    public class RTL_CheckButton : DevExpress.XtraEditors.CheckButton
    {
        const int WS_EX_LAYOUTRTL = 0x400000;
        const int WS_EX_RTLREADING = 0x2000;

        private bool _mirrored = false;

        [Description("Change to the right-to-left layout."), DefaultValue(false), Localizable(true), Category("Appearance"), Browsable(true)]
        public bool Mirrored
        {
            get
            {
                return _mirrored;
            }
            set
            {
                if (_mirrored != value)
                {
                    _mirrored = value;
                    this.Refresh();
                    base.OnRightToLeftChanged(EventArgs.Empty);
                }
            }
        }

        protected override CreateParams CreateParams
        {
            get
            {
                if (this.Mirrored)
                {
                    CreateParams CP;
                    CP = base.CreateParams;
                    CP.ExStyle = CP.ExStyle | WS_EX_LAYOUTRTL | WS_EX_RTLREADING;
                    return CP;
                }
                else
                {
                    return base.CreateParams;
                }
            }
        }
    }

    public class RTL_CheckedComboBoxEdit : DevExpress.XtraEditors.CheckedComboBoxEdit
    {
        const int WS_EX_LAYOUTRTL = 0x400000;
        const int WS_EX_RTLREADING = 0x2000;

        private bool _mirrored = false;

        [Description("Change to the right-to-left layout."), DefaultValue(false), Localizable(true), Category("Appearance"), Browsable(true)]
        public bool Mirrored
        {
            get
            {
                return _mirrored;
            }
            set
            {
                if (_mirrored != value)
                {
                    _mirrored = value;
                    this.Refresh();
                    base.OnRightToLeftChanged(EventArgs.Empty);
                }
            }
        }

        protected override CreateParams CreateParams
        {
            get
            {
                if (this.Mirrored)
                {
                    CreateParams CP;
                    CP = base.CreateParams;
                    CP.ExStyle = CP.ExStyle | WS_EX_LAYOUTRTL | WS_EX_RTLREADING;
                    return CP;
                }
                else
                {
                    return base.CreateParams;
                }
            }
        }      
    }

    public class RTL_CheckEdit : DevExpress.XtraEditors.CheckEdit
    {
        const int WS_EX_LAYOUTRTL = 0x400000;
        const int WS_EX_RTLREADING = 0x2000;

        private bool _mirrored = false;

        [Description("Change to the right-to-left layout."), DefaultValue(false), Localizable(true), Category("Appearance"), Browsable(true)]
        public bool Mirrored
        {
            get
            {
                return _mirrored;
            }
            set
            {
                if (_mirrored != value)
                {
                    _mirrored = value;
                    this.Refresh();
                    base.OnRightToLeftChanged(EventArgs.Empty);
                }
            }
        }

        protected override CreateParams CreateParams
        {
            get
            {
                if (this.Mirrored)
                {
                    CreateParams CP;
                    CP = base.CreateParams;
                    CP.ExStyle = CP.ExStyle | WS_EX_LAYOUTRTL | WS_EX_RTLREADING;
                    return CP;
                }
                else
                {
                    return base.CreateParams;
                }
            }
        }
    }

    public class RTL_ColorEdit : DevExpress.XtraEditors.ColorEdit
    {
        const int WS_EX_LAYOUTRTL = 0x400000;
        const int WS_EX_RTLREADING = 0x2000;

        private bool _mirrored = false;

        [Description("Change to the right-to-left layout."), DefaultValue(false), Localizable(true), Category("Appearance"), Browsable(true)]
        public bool Mirrored
        {
            get
            {
                return _mirrored;
            }
            set
            {
                if (_mirrored != value)
                {
                    _mirrored = value;
                    this.Refresh();
                    base.OnRightToLeftChanged(EventArgs.Empty);
                }
            }
        }

        protected override CreateParams CreateParams
        {
            get
            {
                if (this.Mirrored)
                {
                    CreateParams CP;
                    CP = base.CreateParams;
                    CP.ExStyle = CP.ExStyle | WS_EX_LAYOUTRTL | WS_EX_RTLREADING;
                    return CP;
                }
                else
                {
                    return base.CreateParams;
                }
            }
        }
    }

    public class RTL_ComboBoxEdit : DevExpress.XtraEditors.ComboBoxEdit
    {
        const int WS_EX_LAYOUTRTL = 0x400000;
        const int WS_EX_RTLREADING = 0x2000;

        private bool _mirrored = false;
        
        [Description("Change to the right-to-left layout."), DefaultValue(false), Localizable(true), Category("Appearance"), Browsable(true)]
        public bool Mirrored
        {
            get
            {
                return _mirrored;
            }
            set
            {
                if (_mirrored != value)
                {
                    _mirrored = value;
                    this.Refresh();
                    base.OnRightToLeftChanged(EventArgs.Empty);
                }
            }
        }

        protected override CreateParams CreateParams
        {
            get
            {
                if (this.Mirrored)
                {
                    CreateParams CP;
                    CP = base.CreateParams;
                    CP.ExStyle = CP.ExStyle | WS_EX_LAYOUTRTL | WS_EX_RTLREADING;
                    return CP;
                }
                else
                {
                    return base.CreateParams;
                }
            }
        }
    }

    public class RTL_DataNavigator : DevExpress.XtraEditors.DataNavigator
    {
        const int WS_EX_LAYOUTRTL = 0x400000;
        const int WS_EX_RTLREADING = 0x2000;

        private bool _mirrored = false;

        [Description("Change to the right-to-left layout."), DefaultValue(false), Localizable(true), Category("Appearance"), Browsable(true)]
        public bool Mirrored
        {
            get
            {
                return _mirrored;
            }
            set
            {
                if (_mirrored != value)
                {
                    _mirrored = value;
                    this.Refresh();
                    base.OnRightToLeftChanged(EventArgs.Empty);
                }
            }
        }

        protected override CreateParams CreateParams
        {
            get
            {
                if (this.Mirrored)
                {
                    CreateParams CP;
                    CP = base.CreateParams;
                    CP.ExStyle = CP.ExStyle | WS_EX_LAYOUTRTL | WS_EX_RTLREADING;
                    return CP;
                }
                else
                {
                    return base.CreateParams;
                }
            }
        }
    }

    public class RTL_DateEdit : DevExpress.XtraEditors.DateEdit
    {
        const int WS_EX_LAYOUTRTL = 0x400000;
        const int WS_EX_RTLREADING = 0x2000;

        private bool _mirrored = false;

        [Description("Change to the right-to-left layout."), DefaultValue(false), Localizable(true), Category("Appearance"), Browsable(true)]
        public bool Mirrored
        {
            get
            {
                return _mirrored;
            }
            set
            {
                if (_mirrored != value)
                {
                    _mirrored = value;
                    this.Refresh();
                    base.OnRightToLeftChanged(EventArgs.Empty);
                }
            }
        }

        protected override CreateParams CreateParams
        {
            get
            {
                if (this.Mirrored)
                {
                    CreateParams CP;
                    CP = base.CreateParams;
                    CP.ExStyle = CP.ExStyle | WS_EX_LAYOUTRTL | WS_EX_RTLREADING;
                    return CP;
                }
                else
                {
                    return base.CreateParams;
                }
            }
        }
    }

    public class RTL_DropDownButton : DevExpress.XtraEditors.DropDownButton
    {
        const int WS_EX_LAYOUTRTL = 0x400000;
        const int WS_EX_RTLREADING = 0x2000;

        private bool _mirrored = false;

        [Description("Change to the right-to-left layout."), DefaultValue(false), Localizable(true), Category("Appearance"), Browsable(true)]
        public bool Mirrored
        {
            get
            {
                return _mirrored;
            }
            set
            {
                if (_mirrored != value)
                {
                    _mirrored = value;
                    this.Refresh();
                    base.OnRightToLeftChanged(EventArgs.Empty);
                }
            }
        }

        protected override CreateParams CreateParams
        {
            get
            {
                if (this.Mirrored)
                {
                    CreateParams CP;
                    CP = base.CreateParams;
                    CP.ExStyle = CP.ExStyle | WS_EX_LAYOUTRTL | WS_EX_RTLREADING;
                    return CP;
                }
                else
                {
                    return base.CreateParams;
                }
            }
        }
    }

    public class RTL_FilterControl : DevExpress.XtraEditors.FilterControl
    {
        const int WS_EX_LAYOUTRTL = 0x400000;
        const int WS_EX_RTLREADING = 0x2000;

        private bool _mirrored = false;

        [Description("Change to the right-to-left layout."), DefaultValue(false), Localizable(true), Category("Appearance"), Browsable(true)]
        public bool Mirrored
        {
            get
            {
                return _mirrored;
            }
            set
            {
                if (_mirrored != value)
                {
                    _mirrored = value;
                    this.Refresh();
                    base.OnRightToLeftChanged(EventArgs.Empty);
                }
            }
        }

        protected override CreateParams CreateParams
        {
            get
            {
                if (this.Mirrored)
                {
                    CreateParams CP;
                    CP = base.CreateParams;
                    CP.ExStyle = CP.ExStyle | WS_EX_LAYOUTRTL | WS_EX_RTLREADING;
                    return CP;
                }
                else
                {
                    return base.CreateParams;
                }
            }
        }
    }

    public class RTL_FontEdit : DevExpress.XtraEditors.FontEdit
    {
        const int WS_EX_LAYOUTRTL = 0x400000;
        const int WS_EX_RTLREADING = 0x2000;

        private bool _mirrored = false;

        [Description("Change to the right-to-left layout."), DefaultValue(false), Localizable(true), Category("Appearance"), Browsable(true)]
        public bool Mirrored
        {
            get
            {
                return _mirrored;
            }
            set
            {
                if (_mirrored != value)
                {
                    _mirrored = value;
                    this.Refresh();
                    base.OnRightToLeftChanged(EventArgs.Empty);
                }
            }
        }

        protected override CreateParams CreateParams
        {
            get
            {
                if (this.Mirrored)
                {
                    CreateParams CP;
                    CP = base.CreateParams;
                    CP.ExStyle = CP.ExStyle | WS_EX_LAYOUTRTL | WS_EX_RTLREADING;
                    return CP;
                }
                else
                {
                    return base.CreateParams;
                }
            }
        }
    }

    public class RTL_GroupControl : DevExpress.XtraEditors.GroupControl
    {
        const int WS_EX_LAYOUTRTL = 0x400000;
        const int WS_EX_RTLREADING = 0x2000;

        private bool _mirrored = false;

        [Description("Change to the right-to-left layout."), DefaultValue(false), Localizable(true), Category("Appearance"), Browsable(true)]
        public bool Mirrored
        {
            get
            {
                return _mirrored;
            }
            set
            {
                if (_mirrored != value)
                {
                    _mirrored = value;
                    this.Refresh();
                    base.OnRightToLeftChanged(EventArgs.Empty);
                }
            }
        }

        protected override CreateParams CreateParams
        {
            get
            {
                if (this.Mirrored)
                {
                    CreateParams CP;
                    CP = base.CreateParams;
                    CP.ExStyle = CP.ExStyle | WS_EX_LAYOUTRTL | WS_EX_RTLREADING;
                    return CP;
                }
                else
                {
                    return base.CreateParams;
                }
            }
        }
    }

    public class RTL_HyperLinkEdit : DevExpress.XtraEditors.HyperLinkEdit
    {
        const int WS_EX_LAYOUTRTL = 0x400000;
        const int WS_EX_RTLREADING = 0x2000;

        private bool _mirrored = false;

        [Description("Change to the right-to-left layout."), DefaultValue(false), Localizable(true), Category("Appearance"), Browsable(true)]
        public bool Mirrored
        {
            get
            {
                return _mirrored;
            }
            set
            {
                if (_mirrored != value)
                {
                    _mirrored = value;
                    this.Refresh();
                    base.OnRightToLeftChanged(EventArgs.Empty);
                }
            }
        }

        protected override CreateParams CreateParams
        {
            get
            {
                if (this.Mirrored)
                {
                    CreateParams CP;
                    CP = base.CreateParams;
                    CP.ExStyle = CP.ExStyle | WS_EX_LAYOUTRTL | WS_EX_RTLREADING;
                    return CP;
                }
                else
                {
                    return base.CreateParams;
                }
            }
        }
    }

    public class RTL_ImageComboBoxEdit: DevExpress.XtraEditors.ImageComboBoxEdit
    {
        const int WS_EX_LAYOUTRTL = 0x400000;
        const int WS_EX_RTLREADING = 0x2000;

        private bool _mirrored = false;

        [Description("Change to the right-to-left layout."), DefaultValue(false), Localizable(true), Category("Appearance"), Browsable(true)]
        public bool Mirrored
        {
            get
            {
                return _mirrored;
            }
            set
            {
                if (_mirrored != value)
                {
                    _mirrored = value;
                    this.Refresh();
                    base.OnRightToLeftChanged(EventArgs.Empty);
                }
            }
        }

        protected override CreateParams CreateParams
        {
            get
            {
                if (this.Mirrored)
                {
                    CreateParams CP;
                    CP = base.CreateParams;
                    CP.ExStyle = CP.ExStyle | WS_EX_LAYOUTRTL | WS_EX_RTLREADING;
                    return CP;
                }
                else
                {
                    return base.CreateParams;
                }
            }
        }
    }

    public class RTL_ImageEdit : DevExpress.XtraEditors.ImageEdit
    {
        const int WS_EX_LAYOUTRTL = 0x400000;
        const int WS_EX_RTLREADING = 0x2000;

        private bool _mirrored = false;

        [Description("Change to the right-to-left layout."), DefaultValue(false), Localizable(true), Category("Appearance"), Browsable(true)]
        public bool Mirrored
        {
            get
            {
                return _mirrored;
            }
            set
            {
                if (_mirrored != value)
                {
                    _mirrored = value;
                    this.Refresh();
                    base.OnRightToLeftChanged(EventArgs.Empty);
                }
            }
        }

        protected override CreateParams CreateParams
        {
            get
            {
                if (this.Mirrored)
                {
                    CreateParams CP;
                    CP = base.CreateParams;
                    CP.ExStyle = CP.ExStyle | WS_EX_LAYOUTRTL | WS_EX_RTLREADING;
                    return CP;
                }
                else
                {
                    return base.CreateParams;
                }
            }
        }
    }

    public class RTL_LabelControl : DevExpress.XtraEditors.LabelControl
    {
        const int WS_EX_LAYOUTRTL = 0x400000;
        const int WS_EX_RTLREADING = 0x2000;

        private bool _mirrored = false;

        [Description("Change to the right-to-left layout."), DefaultValue(false), Localizable(true), Category("Appearance"), Browsable(true)]
        public bool Mirrored
        {
            get
            {
                return _mirrored;
            }
            set
            {
                if (_mirrored != value)
                {
                    _mirrored = value;
                    this.Refresh();
                    base.OnRightToLeftChanged(EventArgs.Empty);
                }
            }
        }

        protected override CreateParams CreateParams
        {
            get
            {
                if (this.Mirrored)
                {
                    CreateParams CP;
                    CP = base.CreateParams;
                    CP.ExStyle = CP.ExStyle | WS_EX_LAYOUTRTL | WS_EX_RTLREADING;
                    return CP;
                }
                else
                {
                    return base.CreateParams;
                }
            }
        }
    }

    public class RTL_ListBoxControl : DevExpress.XtraEditors.ListBoxControl
    {
        const int WS_EX_LAYOUTRTL = 0x400000;
        const int WS_EX_RTLREADING = 0x2000;

        private bool _mirrored = false;

        [Description("Change to the right-to-left layout."), DefaultValue(false), Localizable(true), Category("Appearance"), Browsable(true)]
        public bool Mirrored
        {
            get
            {
                return _mirrored;
            }
            set
            {
                if (_mirrored != value)
                {
                    _mirrored = value;
                    this.Refresh();
                    base.OnRightToLeftChanged(EventArgs.Empty);
                }
            }
        }

        protected override CreateParams CreateParams
        {
            get
            {
                if (this.Mirrored)
                {
                    CreateParams CP;
                    CP = base.CreateParams;
                    CP.ExStyle = CP.ExStyle | WS_EX_LAYOUTRTL | WS_EX_RTLREADING;
                    return CP;
                }
                else
                {
                    return base.CreateParams;
                }
            }
        }
    }

    public class RTL_LookUpEdit : DevExpress.XtraEditors.LookUpEdit
    {
        const int WS_EX_LAYOUTRTL = 0x400000;
        const int WS_EX_RTLREADING = 0x2000;

        private bool _mirrored = false;

        [Description("Change to the right-to-left layout."), DefaultValue(false), Localizable(true), Category("Appearance"), Browsable(true)]
        public bool Mirrored
        {
            get
            {
                return _mirrored;
            }
            set
            {
                if (_mirrored != value)
                {
                    _mirrored = value;
                    this.Refresh();
                    base.OnRightToLeftChanged(EventArgs.Empty);
                }
            }
        }

        protected override CreateParams CreateParams
        {
            get
            {
                if (this.Mirrored)
                {
                    CreateParams CP;
                    CP = base.CreateParams;
                    CP.ExStyle = CP.ExStyle | WS_EX_LAYOUTRTL | WS_EX_RTLREADING;
                    return CP;
                }
                else
                {
                    return base.CreateParams;
                }
            }
        }
    }

    public class RTL_MarqueeProgressBarControl : DevExpress.XtraEditors.MarqueeProgressBarControl
    {
        const int WS_EX_LAYOUTRTL = 0x400000;
        const int WS_EX_RTLREADING = 0x2000;

        private bool _mirrored = false;

        [Description("Change to the right-to-left layout."), DefaultValue(false), Localizable(true), Category("Appearance"), Browsable(true)]
        public bool Mirrored
        {
            get
            {
                return _mirrored;
            }
            set
            {
                if (_mirrored != value)
                {
                    _mirrored = value;
                    this.Refresh();
                    base.OnRightToLeftChanged(EventArgs.Empty);
                }
            }
        }

        protected override CreateParams CreateParams
        {
            get
            {
                if (this.Mirrored)
                {
                    CreateParams CP;
                    CP = base.CreateParams;
                    CP.ExStyle = CP.ExStyle | WS_EX_LAYOUTRTL | WS_EX_RTLREADING;
                    return CP;
                }
                else
                {
                    return base.CreateParams;
                }
            }
        }
    }

    public class RTL_MemoEdit : DevExpress.XtraEditors.MemoEdit
    {
        const int WS_EX_LAYOUTRTL = 0x400000;
        const int WS_EX_RTLREADING = 0x2000;

        private bool _mirrored = false;

        [Description("Change to the right-to-left layout."), DefaultValue(false), Localizable(true), Category("Appearance"), Browsable(true)]
        public bool Mirrored
        {
            get
            {
                return _mirrored;
            }
            set
            {
                if (_mirrored != value)
                {
                    _mirrored = value;
                    this.Refresh();
                    base.OnRightToLeftChanged(EventArgs.Empty);
                }
            }
        }

        protected override CreateParams CreateParams
        {
            get
            {
                if (this.Mirrored)
                {
                    CreateParams CP;
                    CP = base.CreateParams;
                    CP.ExStyle = CP.ExStyle | WS_EX_LAYOUTRTL | WS_EX_RTLREADING;
                    return CP;
                }
                else
                {
                    return base.CreateParams;
                }
            }
        }
    }

    public class RTL_MemoExEdit : DevExpress.XtraEditors.MemoExEdit
    {
        const int WS_EX_LAYOUTRTL = 0x400000;
        const int WS_EX_RTLREADING = 0x2000;

        private bool _mirrored = false;

        [Description("Change to the right-to-left layout."), DefaultValue(false), Localizable(true), Category("Appearance"), Browsable(true)]
        public bool Mirrored
        {
            get
            {
                return _mirrored;
            }
            set
            {
                if (_mirrored != value)
                {
                    _mirrored = value;
                    this.Refresh();
                    base.OnRightToLeftChanged(EventArgs.Empty);
                }
            }
        }

        protected override CreateParams CreateParams
        {
            get
            {
                if (this.Mirrored)
                {
                    CreateParams CP;
                    CP = base.CreateParams;
                    CP.ExStyle = CP.ExStyle | WS_EX_LAYOUTRTL | WS_EX_RTLREADING;
                    return CP;
                }
                else
                {
                    return base.CreateParams;
                }
            }
        }
    }

    public class RTL_MRUEdit : DevExpress.XtraEditors.MRUEdit
    {
        const int WS_EX_LAYOUTRTL = 0x400000;
        const int WS_EX_RTLREADING = 0x2000;

        private bool _mirrored = false;

        [Description("Change to the right-to-left layout."), DefaultValue(false), Localizable(true), Category("Appearance"), Browsable(true)]
        public bool Mirrored
        {
            get
            {
                return _mirrored;
            }
            set
            {
                if (_mirrored != value)
                {
                    _mirrored = value;
                    this.Refresh();
                    base.OnRightToLeftChanged(EventArgs.Empty);
                }
            }
        }

        protected override CreateParams CreateParams
        {
            get
            {
                if (this.Mirrored)
                {
                    CreateParams CP;
                    CP = base.CreateParams;
                    CP.ExStyle = CP.ExStyle | WS_EX_LAYOUTRTL | WS_EX_RTLREADING;
                    return CP;
                }
                else
                {
                    return base.CreateParams;
                }
            }
        }
    }

    public class RTL_PanelControl : DevExpress.XtraEditors.PanelControl
    {
        const int WS_EX_LAYOUTRTL = 0x400000;
        const int WS_EX_RTLREADING = 0x2000;

        private bool _mirrored = false;

        [Description("Change to the right-to-left layout."), DefaultValue(false), Localizable(true), Category("Appearance"), Browsable(true)]
        public bool Mirrored
        {
            get
            {
                return _mirrored;
            }
            set
            {
                if (_mirrored != value)
                {
                    _mirrored = value;
                    this.Refresh();
                    base.OnRightToLeftChanged(EventArgs.Empty);
                }
            }
        }

        protected override CreateParams CreateParams
        {
            get
            {
                if (this.Mirrored)
                {
                    CreateParams CP;
                    CP = base.CreateParams;
                    CP.ExStyle = CP.ExStyle | WS_EX_LAYOUTRTL | WS_EX_RTLREADING;
                    return CP;
                }
                else
                {
                    return base.CreateParams;
                }
            }
        }
    }

    public class RTL_PopupContainerEdit : DevExpress.XtraEditors.PopupContainerEdit
    {
        const int WS_EX_LAYOUTRTL = 0x400000;
        const int WS_EX_RTLREADING = 0x2000;

        private bool _mirrored = false;

        [Description("Change to the right-to-left layout."), DefaultValue(false), Localizable(true), Category("Appearance"), Browsable(true)]
        public bool Mirrored
        {
            get
            {
                return _mirrored;
            }
            set
            {
                if (_mirrored != value)
                {
                    _mirrored = value;
                    this.Refresh();
                    base.OnRightToLeftChanged(EventArgs.Empty);
                }
            }
        }

        protected override CreateParams CreateParams
        {
            get
            {
                if (this.Mirrored)
                {
                    CreateParams CP;
                    CP = base.CreateParams;
                    CP.ExStyle = CP.ExStyle | WS_EX_LAYOUTRTL | WS_EX_RTLREADING;
                    return CP;
                }
                else
                {
                    return base.CreateParams;
                }
            }
        }

        protected override PopupBaseForm CreatePopupForm()
        {
            if (Properties.PopupControl == null) return null;
            return new RTL_PopupContainerForm(this);
        }
    }

    public class RTL_ProgressBarControl : DevExpress.XtraEditors.ProgressBarControl
    {
        const int WS_EX_LAYOUTRTL = 0x400000;
        const int WS_EX_RTLREADING = 0x2000;

        private bool _mirrored = false;

        [Description("Change to the right-to-left layout."), DefaultValue(false), Localizable(true), Category("Appearance"), Browsable(true)]
        public bool Mirrored
        {
            get
            {
                return _mirrored;
            }
            set
            {
                if (_mirrored != value)
                {
                    _mirrored = value;
                    this.Refresh();
                    base.OnRightToLeftChanged(EventArgs.Empty);
                }
            }
        }

        protected override CreateParams CreateParams
        {
            get
            {
                if (this.Mirrored)
                {
                    CreateParams CP;
                    CP = base.CreateParams;
                    CP.ExStyle = CP.ExStyle | WS_EX_LAYOUTRTL | WS_EX_RTLREADING;
                    return CP;
                }
                else
                {
                    return base.CreateParams;
                }
            }
        }
    }

    public class RTL_RadioGroup : DevExpress.XtraEditors.RadioGroup
    {
        const int WS_EX_LAYOUTRTL = 0x400000;
        const int WS_EX_RTLREADING = 0x2000;

        private bool _mirrored = false;

        [Description("Change to the right-to-left layout."), DefaultValue(false), Localizable(true), Category("Appearance"), Browsable(true)]
        public bool Mirrored
        {
            get
            {
                return _mirrored;
            }
            set
            {
                if (_mirrored != value)
                {
                    _mirrored = value;
                    this.Refresh();
                    base.OnRightToLeftChanged(EventArgs.Empty);
                }
            }
        }

        protected override CreateParams CreateParams
        {
            get
            {
                if (this.Mirrored)
                {
                    CreateParams CP;
                    CP = base.CreateParams;
                    CP.ExStyle = CP.ExStyle | WS_EX_LAYOUTRTL | WS_EX_RTLREADING;
                    return CP;
                }
                else
                {
                    return base.CreateParams;
                }
            }
        }
    }

    public class RTL_RangeTrackBarControl : DevExpress.XtraEditors.RangeTrackBarControl
    {
        const int WS_EX_LAYOUTRTL = 0x400000;
        const int WS_EX_RTLREADING = 0x2000;

        private bool _mirrored = false;

        [Description("Change to the right-to-left layout."), DefaultValue(false), Localizable(true), Category("Appearance"), Browsable(true)]
        public bool Mirrored
        {
            get
            {
                return _mirrored;
            }
            set
            {
                if (_mirrored != value)
                {
                    _mirrored = value;
                    this.Refresh();
                    base.OnRightToLeftChanged(EventArgs.Empty);
                }
            }
        }

        protected override CreateParams CreateParams
        {
            get
            {
                if (this.Mirrored)
                {
                    CreateParams CP;
                    CP = base.CreateParams;
                    CP.ExStyle = CP.ExStyle | WS_EX_LAYOUTRTL | WS_EX_RTLREADING;
                    return CP;
                }
                else
                {
                    return base.CreateParams;
                }
            }
        }
    }

    public class RTL_SimpleButton : DevExpress.XtraEditors.SimpleButton
    {
        const int WS_EX_LAYOUTRTL = 0x400000;
        const int WS_EX_RTLREADING = 0x2000;

        private bool _mirrored = false;

        [Description("Change to the right-to-left layout."), DefaultValue(false), Localizable(true), Category("Appearance"), Browsable(true)]
        public bool Mirrored
        {
            get
            {
                return _mirrored;
            }
            set
            {
                if (_mirrored != value)
                {
                    _mirrored = value;
                    this.Refresh();
                    base.OnRightToLeftChanged(EventArgs.Empty);
                }
            }
        }

        protected override CreateParams CreateParams
        {
            get
            {
                if (this.Mirrored)
                {
                    CreateParams CP;
                    CP = base.CreateParams;
                    CP.ExStyle = CP.ExStyle | WS_EX_LAYOUTRTL | WS_EX_RTLREADING;
                    return CP;
                }
                else
                {
                    return base.CreateParams;
                }
            }
        }
    }

    public class RTL_SpinEdit : DevExpress.XtraEditors.SpinEdit
    {
        const int WS_EX_LAYOUTRTL = 0x400000;
        const int WS_EX_RTLREADING = 0x2000;

        private bool _mirrored = false;

        [Description("Change to the right-to-left layout."), DefaultValue(false), Localizable(true), Category("Appearance"), Browsable(true)]
        public bool Mirrored
        {
            get
            {
                return _mirrored;
            }
            set
            {
                if (_mirrored != value)
                {
                    _mirrored = value;
                    this.Refresh();
                    base.OnRightToLeftChanged(EventArgs.Empty);
                }
            }
        }

        protected override CreateParams CreateParams
        {
            get
            {
                if (this.Mirrored)
                {
                    CreateParams CP;
                    CP = base.CreateParams;
                    CP.ExStyle = CP.ExStyle | WS_EX_LAYOUTRTL | WS_EX_RTLREADING;
                    return CP;
                }
                else
                {
                    return base.CreateParams;
                }
            }
        }
    }

    public class RTL_SplitContainerControl : DevExpress.XtraEditors.SplitContainerControl
    {
        const int WS_EX_LAYOUTRTL = 0x400000;
        const int WS_EX_RTLREADING = 0x2000;

        private bool _mirrored = false;

        [Description("Change to the right-to-left layout."), DefaultValue(false), Localizable(true), Category("Appearance"), Browsable(true)]
        public bool Mirrored
        {
            get
            {
                return _mirrored;
            }
            set
            {
                if (_mirrored != value)
                {
                    _mirrored = value;
                    this.Refresh();
                    base.OnRightToLeftChanged(EventArgs.Empty);
                }
            }
        }

        protected override CreateParams CreateParams
        {
            get
            {
                if (this.Mirrored)
                {
                    CreateParams CP;
                    CP = base.CreateParams;
                    CP.ExStyle = CP.ExStyle | WS_EX_LAYOUTRTL | WS_EX_RTLREADING;
                    return CP;
                }
                else
                {
                    return base.CreateParams;
                }
            }
        }
    }

    public class RTL_SplitterControl : DevExpress.XtraEditors.SplitterControl
    {
        const int WS_EX_LAYOUTRTL = 0x400000;
        const int WS_EX_RTLREADING = 0x2000;

        private bool _mirrored = false;

        [Description("Change to the right-to-left layout."), DefaultValue(false), Localizable(true), Category("Appearance"), Browsable(true)]
        public bool Mirrored
        {
            get
            {
                return _mirrored;
            }
            set
            {
                if (_mirrored != value)
                {
                    _mirrored = value;
                    this.Refresh();
                    base.OnRightToLeftChanged(EventArgs.Empty);
                }
            }
        }

        protected override CreateParams CreateParams
        {
            get
            {
                if (this.Mirrored)
                {
                    CreateParams CP;
                    CP = base.CreateParams;
                    CP.ExStyle = CP.ExStyle | WS_EX_LAYOUTRTL | WS_EX_RTLREADING;
                    return CP;
                }
                else
                {
                    return base.CreateParams;
                }
            }
        }
    }

    public class RTL_TextEdit : DevExpress.XtraEditors.TextEdit
    {
        const int WS_EX_LAYOUTRTL = 0x400000;
        const int WS_EX_RTLREADING = 0x2000;

        private bool _mirrored = false;

        [Description("Change to the right-to-left layout."), DefaultValue(false), Localizable(true), Category("Appearance"), Browsable(true)]
        public bool Mirrored
        {
            get
            {
                return _mirrored;
            }
            set
            {
                if (_mirrored != value)
                {
                    _mirrored = value;
                    this.Refresh();
                    base.OnRightToLeftChanged(EventArgs.Empty);
                }
            }
        }

        protected override CreateParams CreateParams
        {
            get
            {
                if (this.Mirrored)
                {
                    CreateParams CP;
                    CP = base.CreateParams;
                    CP.ExStyle = CP.ExStyle | WS_EX_LAYOUTRTL | WS_EX_RTLREADING;
                    return CP;
                }
                else
                {
                    return base.CreateParams;
                }
            }
        }
    }

    public class RTL_TimeEdit : DevExpress.XtraEditors.TimeEdit
    {
        const int WS_EX_LAYOUTRTL = 0x400000;
        const int WS_EX_RTLREADING = 0x2000;

        private bool _mirrored = false;

        [Description("Change to the right-to-left layout."), DefaultValue(false), Localizable(true), Category("Appearance"), Browsable(true)]
        public bool Mirrored
        {
            get
            {
                return _mirrored;
            }
            set
            {
                if (_mirrored != value)
                {
                    _mirrored = value;
                    this.Refresh();
                    base.OnRightToLeftChanged(EventArgs.Empty);
                }
            }
        }

        protected override CreateParams CreateParams
        {
            get
            {
                if (this.Mirrored)
                {
                    CreateParams CP;
                    CP = base.CreateParams;
                    CP.ExStyle = CP.ExStyle | WS_EX_LAYOUTRTL | WS_EX_RTLREADING;
                    return CP;
                }
                else
                {
                    return base.CreateParams;
                }
            }
        }
    }

    public class RTL_TrackBarControl : DevExpress.XtraEditors.TrackBarControl
    {
        const int WS_EX_LAYOUTRTL = 0x400000;
        const int WS_EX_RTLREADING = 0x2000;

        private bool _mirrored = false;

        [Description("Change to the right-to-left layout."), DefaultValue(false), Localizable(true), Category("Appearance"), Browsable(true)]
        public bool Mirrored
        {
            get
            {
                return _mirrored;
            }
            set
            {
                if (_mirrored != value)
                {
                    _mirrored = value;
                    this.Refresh();
                    base.OnRightToLeftChanged(EventArgs.Empty);
                }
            }
        }

        protected override CreateParams CreateParams
        {
            get
            {
                if (this.Mirrored)
                {
                    CreateParams CP;
                    CP = base.CreateParams;
                    CP.ExStyle = CP.ExStyle | WS_EX_LAYOUTRTL | WS_EX_RTLREADING;
                    return CP;
                }
                else
                {
                    return base.CreateParams;
                }
            }
        }
    }

    public class RTL_VScrollBar : DevExpress.XtraEditors.VScrollBar
    {
        const int WS_EX_LAYOUTRTL = 0x400000;
        const int WS_EX_RTLREADING = 0x2000;

        private bool _mirrored = false;

        [Description("Change to the right-to-left layout."), DefaultValue(false), Localizable(true), Category("Appearance"), Browsable(true)]
        public bool Mirrored
        {
            get
            {
                return _mirrored;
            }
            set
            {
                if (_mirrored != value)
                {
                    _mirrored = value;
                    this.Refresh();
                    base.OnRightToLeftChanged(EventArgs.Empty);
                }
            }
        }

        protected override CreateParams CreateParams
        {
            get
            {
                if (this.Mirrored)
                {
                    CreateParams CP;
                    CP = base.CreateParams;
                    CP.ExStyle = CP.ExStyle | WS_EX_LAYOUTRTL | WS_EX_RTLREADING;
                    return CP;
                }
                else
                {
                    return base.CreateParams;
                }
            }
        }
    }

    public class RTL_CheckedListBoxControl : DevExpress.XtraEditors.CheckedListBoxControl
    {
        const int WS_EX_LAYOUTRTL = 0x400000;
        const int WS_EX_RTLREADING = 0x2000;

        private bool _mirrored = false;

        [Description("Change to the right-to-left layout."), DefaultValue(false), Localizable(true), Category("Appearance"), Browsable(true)]
        public bool Mirrored
        {
            get
            {
                return _mirrored;
            }
            set
            {
                if (_mirrored != value)
                {
                    _mirrored = value;
                    this.Refresh();
                    base.OnRightToLeftChanged(EventArgs.Empty);
                }
            }
        }

        protected override CreateParams CreateParams
        {
            get
            {
                if (this.Mirrored)
                {
                    CreateParams CP;
                    CP = base.CreateParams;
                    CP.ExStyle = CP.ExStyle | WS_EX_LAYOUTRTL | WS_EX_RTLREADING;
                    return CP;
                }
                else
                {
                    return base.CreateParams;
                }
            }
        }
    }

    public class RTL_PopupContainerControl : DevExpress.XtraEditors.PopupContainerControl
    {
        const int WS_EX_LAYOUTRTL = 0x400000;
        const int WS_EX_RTLREADING = 0x2000;

        private bool _mirrored = false;

        [Description("Change to the right-to-left layout."), DefaultValue(false), Localizable(true), Category("Appearance"), Browsable(true)]
        public bool Mirrored
        {
            get
            {
                return _mirrored;
            }
            set
            {
                if (_mirrored != value)
                {
                    _mirrored = value;
                    this.Refresh();
                    base.OnRightToLeftChanged(EventArgs.Empty);
                }
            }
        }

        protected override CreateParams CreateParams
        {
            get
            {
                if (this.Mirrored)
                {
                    CreateParams CP;
                    CP = base.CreateParams;
                    CP.ExStyle = CP.ExStyle | WS_EX_LAYOUTRTL | WS_EX_RTLREADING;
                    return CP;
                }
                else
                {
                    return base.CreateParams;
                }
            }
        }
    }

    public class RTL_PopupContainerForm : DevExpress.XtraEditors.Popup.PopupContainerForm
    {
        const int WS_EX_LAYOUTRTL = 0x400000;
        const int WS_EX_RTLREADING = 0x2000;

        public RTL_PopupContainerForm(DevExpress.XtraEditors.PopupContainerEdit ownerEdit)
            : base(ownerEdit)
        {

        }

        public override void ForceCreateHandle()
        {
            base.ForceCreateHandle();
            Point NewLocation = Location;
            NewLocation.X -= Width;
            Location = NewLocation;
        }

        protected override CreateParams CreateParams
        {
            get
            {
                CreateParams CP;
                CP = base.CreateParams;
                CP.ExStyle = CP.ExStyle | WS_EX_LAYOUTRTL | WS_EX_RTLREADING;
                return CP;
            }
        }
    }

    public class RTL_CalcEdit_2 : DevExpress.XtraEditors.CalcEdit
    {
        const int WS_EX_LAYOUTRTL = 0x400000;
        const int WS_EX_RTLREADING = 0x2000;

        private bool _mirrored = false;

        [Description("Change to the right-to-left layout."), DefaultValue(false), Localizable(true), Category("Appearance"), Browsable(true)]
        public bool Mirrored
        {
            get
            {
                return _mirrored;
            }
            set
            {
                if (_mirrored != value)
                {
                    _mirrored = value;
                    this.Refresh();
                    base.OnRightToLeftChanged(EventArgs.Empty);
                }
            }
        }

        protected override CreateParams CreateParams
        {
            get
            {
                if (this.Mirrored)
                {
                    CreateParams CP;
                    CP = base.CreateParams;
                    CP.ExStyle = CP.ExStyle | WS_EX_LAYOUTRTL | WS_EX_RTLREADING;
                    return CP;
                }
                else
                {
                    return base.CreateParams;
                }
            }
        }
        //protected override DevExpress.XtraEditors.Popup.PopupBaseForm CreatePop
    }

    public class RTL_PopupCalcEditForm : PopupCalcEditForm
    {
        public RTL_PopupCalcEditForm(PopupBaseEdit ownerEdit)
            : base(ownerEdit)
        {
            
        }
        public override void ShowPopupForm()
        {
            Point NewLocation = Location;
            NewLocation.X -= Width;
            Location = NewLocation;
            base.ShowPopupForm();
        }
    }

}
