﻿namespace RTLComponent_Test
{
    partial class XtraForm1_EditorC
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.rtL_PopupContainerControl1 = new RTLComponent_Test.RTL_PopupContainerControl();
            this.rtL_CheckEdit5 = new RTLComponent_Test.RTL_CheckEdit();
            this.rtL_CheckEdit4 = new RTLComponent_Test.RTL_CheckEdit();
            this.rtL_CheckedListBoxControl1 = new RTLComponent_Test.RTL_CheckedListBoxControl();
            this.rtL_VScrollBar1 = new RTLComponent_Test.RTL_VScrollBar();
            this.rtL_TrackBarControl1 = new RTLComponent_Test.RTL_TrackBarControl();
            this.rtL_TimeEdit1 = new RTLComponent_Test.RTL_TimeEdit();
            this.rtL_TextEdit1 = new RTLComponent_Test.RTL_TextEdit();
            this.rtL_SplitContainerControl1 = new RTLComponent_Test.RTL_SplitContainerControl();
            this.rtL_SpinEdit1 = new RTLComponent_Test.RTL_SpinEdit();
            this.rtL_SimpleButton1 = new RTLComponent_Test.RTL_SimpleButton();
            this.rtL_RangeTrackBarControl1 = new RTLComponent_Test.RTL_RangeTrackBarControl();
            this.rtL_RadioGroup1 = new RTLComponent_Test.RTL_RadioGroup();
            this.rtL_ProgressBarControl1 = new RTLComponent_Test.RTL_ProgressBarControl();
            this.rtL_PopupContainerEdit1 = new RTLComponent_Test.RTL_PopupContainerEdit();
            this.rtL_PanelControl1 = new RTLComponent_Test.RTL_PanelControl();
            this.rtL_CheckEdit3 = new RTLComponent_Test.RTL_CheckEdit();
            this.rtL_ComboBoxEdit3 = new RTLComponent_Test.RTL_ComboBoxEdit();
            this.rtL_MRUEdit1 = new RTLComponent_Test.RTL_MRUEdit();
            this.rtL_MemoExEdit1 = new RTLComponent_Test.RTL_MemoExEdit();
            this.rtL_MemoEdit1 = new RTLComponent_Test.RTL_MemoEdit();
            this.rtL_MarqueeProgressBarControl1 = new RTLComponent_Test.RTL_MarqueeProgressBarControl();
            this.rtL_LookUpEdit1 = new RTLComponent_Test.RTL_LookUpEdit();
            this.rtL_ListBoxControl1 = new RTLComponent_Test.RTL_ListBoxControl();
            this.rtL_LabelControl1 = new RTLComponent_Test.RTL_LabelControl();
            this.rtL_ImageEdit1 = new RTLComponent_Test.RTL_ImageEdit();
            this.rtL_ImageComboBoxEdit1 = new RTLComponent_Test.RTL_ImageComboBoxEdit();
            this.rtL_HyperLinkEdit1 = new RTLComponent_Test.RTL_HyperLinkEdit();
            this.rtL_GroupControl1 = new RTLComponent_Test.RTL_GroupControl();
            this.rtL_ComboBoxEdit2 = new RTLComponent_Test.RTL_ComboBoxEdit();
            this.rtL_CheckEdit2 = new RTLComponent_Test.RTL_CheckEdit();
            this.rtL_FontEdit1 = new RTLComponent_Test.RTL_FontEdit();
            this.rtL_FilterControl1 = new RTLComponent_Test.RTL_FilterControl();
            this.rtL_DropDownButton1 = new RTLComponent_Test.RTL_DropDownButton();
            this.rtL_DateEdit1 = new RTLComponent_Test.RTL_DateEdit();
            this.rtL_DataNavigator1 = new RTLComponent_Test.RTL_DataNavigator();
            this.rtL_ComboBoxEdit1 = new RTLComponent_Test.RTL_ComboBoxEdit();
            this.rtL_ColorEdit1 = new RTLComponent_Test.RTL_ColorEdit();
            this.rtL_CheckEdit1 = new RTLComponent_Test.RTL_CheckEdit();
            this.rtL_CheckedComboBoxEdit1 = new RTLComponent_Test.RTL_CheckedComboBoxEdit();
            this.rtL_CheckButton1 = new RTLComponent_Test.RTL_CheckButton();
            this.rtL_CalcEdit1 = new RTLComponent_Test.RTL_CalcEdit();
            this.rtL_ButtonEdit1 = new RTLComponent_Test.RTL_ButtonEdit();
            this.ribbonControl1 = new DevExpress.XtraBars.Ribbon.RibbonControl();
            this.ribbonPage1 = new DevExpress.XtraBars.Ribbon.RibbonPage();
            this.ribbonPageGroup1 = new DevExpress.XtraBars.Ribbon.RibbonPageGroup();
            ((System.ComponentModel.ISupportInitialize)(this.rtL_PopupContainerControl1)).BeginInit();
            this.rtL_PopupContainerControl1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.rtL_CheckEdit5.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.rtL_CheckEdit4.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.rtL_CheckedListBoxControl1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.rtL_TrackBarControl1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.rtL_TrackBarControl1.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.rtL_TimeEdit1.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.rtL_TextEdit1.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.rtL_SplitContainerControl1)).BeginInit();
            this.rtL_SplitContainerControl1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.rtL_SpinEdit1.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.rtL_RangeTrackBarControl1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.rtL_RangeTrackBarControl1.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.rtL_RadioGroup1.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.rtL_ProgressBarControl1.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.rtL_PopupContainerEdit1.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.rtL_PanelControl1)).BeginInit();
            this.rtL_PanelControl1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.rtL_CheckEdit3.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.rtL_ComboBoxEdit3.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.rtL_MRUEdit1.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.rtL_MemoExEdit1.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.rtL_MemoEdit1.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.rtL_MarqueeProgressBarControl1.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.rtL_LookUpEdit1.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.rtL_ListBoxControl1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.rtL_ImageEdit1.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.rtL_ImageComboBoxEdit1.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.rtL_HyperLinkEdit1.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.rtL_GroupControl1)).BeginInit();
            this.rtL_GroupControl1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.rtL_ComboBoxEdit2.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.rtL_CheckEdit2.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.rtL_FontEdit1.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.rtL_DateEdit1.Properties.VistaTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.rtL_DateEdit1.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.rtL_ComboBoxEdit1.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.rtL_ColorEdit1.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.rtL_CheckEdit1.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.rtL_CheckedComboBoxEdit1.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.rtL_CalcEdit1.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.rtL_ButtonEdit1.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ribbonControl1)).BeginInit();
            this.SuspendLayout();
            // 
            // rtL_PopupContainerControl1
            // 
            this.rtL_PopupContainerControl1.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.rtL_PopupContainerControl1.Controls.Add(this.rtL_CheckEdit5);
            this.rtL_PopupContainerControl1.Controls.Add(this.rtL_CheckEdit4);
            this.rtL_PopupContainerControl1.Location = new System.Drawing.Point(453, 396);
            this.rtL_PopupContainerControl1.Mirrored = true;
            this.rtL_PopupContainerControl1.Name = "rtL_PopupContainerControl1";
            this.rtL_PopupContainerControl1.Size = new System.Drawing.Size(210, 96);
            this.rtL_PopupContainerControl1.TabIndex = 36;
            // 
            // rtL_CheckEdit5
            // 
            this.rtL_CheckEdit5.Location = new System.Drawing.Point(3, 24);
            this.rtL_CheckEdit5.Name = "rtL_CheckEdit5";
            this.rtL_CheckEdit5.Properties.Caption = "rtL_CheckEdit5";
            this.rtL_CheckEdit5.Size = new System.Drawing.Size(204, 19);
            this.rtL_CheckEdit5.TabIndex = 1;
            // 
            // rtL_CheckEdit4
            // 
            this.rtL_CheckEdit4.Location = new System.Drawing.Point(3, 3);
            this.rtL_CheckEdit4.Name = "rtL_CheckEdit4";
            this.rtL_CheckEdit4.Properties.Caption = "rtL_CheckEdit4";
            this.rtL_CheckEdit4.Size = new System.Drawing.Size(204, 19);
            this.rtL_CheckEdit4.TabIndex = 0;
            // 
            // rtL_CheckedListBoxControl1
            // 
            this.rtL_CheckedListBoxControl1.Items.AddRange(new DevExpress.XtraEditors.Controls.CheckedListBoxItem[] {
            new DevExpress.XtraEditors.Controls.CheckedListBoxItem(null, "CLBI_1"),
            new DevExpress.XtraEditors.Controls.CheckedListBoxItem(null, "CLBI_2")});
            this.rtL_CheckedListBoxControl1.Location = new System.Drawing.Point(453, 296);
            this.rtL_CheckedListBoxControl1.Name = "rtL_CheckedListBoxControl1";
            this.rtL_CheckedListBoxControl1.Size = new System.Drawing.Size(210, 94);
            this.rtL_CheckedListBoxControl1.TabIndex = 35;
            // 
            // rtL_VScrollBar1
            // 
            this.rtL_VScrollBar1.Location = new System.Drawing.Point(537, 203);
            this.rtL_VScrollBar1.Name = "rtL_VScrollBar1";
            this.rtL_VScrollBar1.Size = new System.Drawing.Size(17, 80);
            this.rtL_VScrollBar1.TabIndex = 34;
            // 
            // rtL_TrackBarControl1
            // 
            this.rtL_TrackBarControl1.EditValue = null;
            this.rtL_TrackBarControl1.Location = new System.Drawing.Point(453, 152);
            this.rtL_TrackBarControl1.Name = "rtL_TrackBarControl1";
            this.rtL_TrackBarControl1.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.rtL_TrackBarControl1.Size = new System.Drawing.Size(210, 45);
            this.rtL_TrackBarControl1.TabIndex = 33;
            // 
            // rtL_TimeEdit1
            // 
            this.rtL_TimeEdit1.EditValue = new System.DateTime(2014, 9, 23, 0, 0, 0, 0);
            this.rtL_TimeEdit1.Location = new System.Drawing.Point(453, 126);
            this.rtL_TimeEdit1.Name = "rtL_TimeEdit1";
            this.rtL_TimeEdit1.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.rtL_TimeEdit1.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.rtL_TimeEdit1.Size = new System.Drawing.Size(210, 20);
            this.rtL_TimeEdit1.TabIndex = 32;
            // 
            // rtL_TextEdit1
            // 
            this.rtL_TextEdit1.EditValue = "i am textedit 1";
            this.rtL_TextEdit1.Location = new System.Drawing.Point(453, 100);
            this.rtL_TextEdit1.Name = "rtL_TextEdit1";
            this.rtL_TextEdit1.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.rtL_TextEdit1.Size = new System.Drawing.Size(210, 20);
            this.rtL_TextEdit1.TabIndex = 31;
            // 
            // rtL_SplitContainerControl1
            // 
            this.rtL_SplitContainerControl1.Location = new System.Drawing.Point(237, 601);
            this.rtL_SplitContainerControl1.Name = "rtL_SplitContainerControl1";
            this.rtL_SplitContainerControl1.Panel1.Text = "Panel1";
            this.rtL_SplitContainerControl1.Panel2.Text = "Panel2";
            this.rtL_SplitContainerControl1.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.rtL_SplitContainerControl1.Size = new System.Drawing.Size(210, 100);
            this.rtL_SplitContainerControl1.SplitterPosition = 76;
            this.rtL_SplitContainerControl1.TabIndex = 30;
            this.rtL_SplitContainerControl1.Text = "rtL_SplitContainerControl1";
            // 
            // rtL_SpinEdit1
            // 
            this.rtL_SpinEdit1.EditValue = new decimal(new int[] {
            0,
            0,
            0,
            0});
            this.rtL_SpinEdit1.Location = new System.Drawing.Point(237, 574);
            this.rtL_SpinEdit1.Name = "rtL_SpinEdit1";
            this.rtL_SpinEdit1.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.rtL_SpinEdit1.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.rtL_SpinEdit1.Size = new System.Drawing.Size(210, 20);
            this.rtL_SpinEdit1.TabIndex = 29;
            // 
            // rtL_SimpleButton1
            // 
            this.rtL_SimpleButton1.Location = new System.Drawing.Point(237, 545);
            this.rtL_SimpleButton1.Name = "rtL_SimpleButton1";
            this.rtL_SimpleButton1.Size = new System.Drawing.Size(210, 23);
            this.rtL_SimpleButton1.TabIndex = 28;
            this.rtL_SimpleButton1.Text = "rtL_SimpleButton1";
            // 
            // rtL_RangeTrackBarControl1
            // 
            this.rtL_RangeTrackBarControl1.EditValue = new DevExpress.XtraEditors.Repository.TrackBarRange(0, 0);
            this.rtL_RangeTrackBarControl1.Location = new System.Drawing.Point(237, 498);
            this.rtL_RangeTrackBarControl1.Name = "rtL_RangeTrackBarControl1";
            this.rtL_RangeTrackBarControl1.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.rtL_RangeTrackBarControl1.Size = new System.Drawing.Size(210, 45);
            this.rtL_RangeTrackBarControl1.TabIndex = 27;
            // 
            // rtL_RadioGroup1
            // 
            this.rtL_RadioGroup1.Location = new System.Drawing.Point(237, 450);
            this.rtL_RadioGroup1.Name = "rtL_RadioGroup1";
            this.rtL_RadioGroup1.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.rtL_RadioGroup1.Size = new System.Drawing.Size(210, 42);
            this.rtL_RadioGroup1.TabIndex = 26;
            // 
            // rtL_ProgressBarControl1
            // 
            this.rtL_ProgressBarControl1.Location = new System.Drawing.Point(237, 422);
            this.rtL_ProgressBarControl1.Name = "rtL_ProgressBarControl1";
            this.rtL_ProgressBarControl1.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.rtL_ProgressBarControl1.Size = new System.Drawing.Size(210, 18);
            this.rtL_ProgressBarControl1.TabIndex = 25;
            // 
            // rtL_PopupContainerEdit1
            // 
            this.rtL_PopupContainerEdit1.Location = new System.Drawing.Point(237, 396);
            this.rtL_PopupContainerEdit1.Name = "rtL_PopupContainerEdit1";
            this.rtL_PopupContainerEdit1.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.rtL_PopupContainerEdit1.Properties.PopupControl = this.rtL_PopupContainerControl1;
            this.rtL_PopupContainerEdit1.Size = new System.Drawing.Size(210, 20);
            this.rtL_PopupContainerEdit1.TabIndex = 24;
            // 
            // rtL_PanelControl1
            // 
            this.rtL_PanelControl1.Controls.Add(this.rtL_CheckEdit3);
            this.rtL_PanelControl1.Controls.Add(this.rtL_ComboBoxEdit3);
            this.rtL_PanelControl1.Location = new System.Drawing.Point(237, 288);
            this.rtL_PanelControl1.Name = "rtL_PanelControl1";
            this.rtL_PanelControl1.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.rtL_PanelControl1.Size = new System.Drawing.Size(210, 102);
            this.rtL_PanelControl1.TabIndex = 23;
            // 
            // rtL_CheckEdit3
            // 
            this.rtL_CheckEdit3.Location = new System.Drawing.Point(5, 31);
            this.rtL_CheckEdit3.Name = "rtL_CheckEdit3";
            this.rtL_CheckEdit3.Properties.Caption = "rtL_CheckEdit3";
            this.rtL_CheckEdit3.Size = new System.Drawing.Size(100, 19);
            this.rtL_CheckEdit3.TabIndex = 24;
            // 
            // rtL_ComboBoxEdit3
            // 
            this.rtL_ComboBoxEdit3.Location = new System.Drawing.Point(5, 5);
            this.rtL_ComboBoxEdit3.Name = "rtL_ComboBoxEdit3";
            this.rtL_ComboBoxEdit3.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.rtL_ComboBoxEdit3.Size = new System.Drawing.Size(98, 20);
            this.rtL_ComboBoxEdit3.TabIndex = 24;
            // 
            // rtL_MRUEdit1
            // 
            this.rtL_MRUEdit1.Location = new System.Drawing.Point(237, 259);
            this.rtL_MRUEdit1.Name = "rtL_MRUEdit1";
            this.rtL_MRUEdit1.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.rtL_MRUEdit1.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.rtL_MRUEdit1.Size = new System.Drawing.Size(210, 20);
            this.rtL_MRUEdit1.TabIndex = 22;
            // 
            // rtL_MemoExEdit1
            // 
            this.rtL_MemoExEdit1.Location = new System.Drawing.Point(237, 233);
            this.rtL_MemoExEdit1.Name = "rtL_MemoExEdit1";
            this.rtL_MemoExEdit1.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.rtL_MemoExEdit1.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.rtL_MemoExEdit1.Size = new System.Drawing.Size(210, 20);
            this.rtL_MemoExEdit1.TabIndex = 21;
            // 
            // rtL_MemoEdit1
            // 
            this.rtL_MemoEdit1.Location = new System.Drawing.Point(237, 124);
            this.rtL_MemoEdit1.Name = "rtL_MemoEdit1";
            this.rtL_MemoEdit1.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.rtL_MemoEdit1.Size = new System.Drawing.Size(210, 103);
            this.rtL_MemoEdit1.TabIndex = 20;
            // 
            // rtL_MarqueeProgressBarControl1
            // 
            this.rtL_MarqueeProgressBarControl1.EditValue = 0;
            this.rtL_MarqueeProgressBarControl1.Location = new System.Drawing.Point(237, 100);
            this.rtL_MarqueeProgressBarControl1.Name = "rtL_MarqueeProgressBarControl1";
            this.rtL_MarqueeProgressBarControl1.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.rtL_MarqueeProgressBarControl1.Size = new System.Drawing.Size(210, 18);
            this.rtL_MarqueeProgressBarControl1.TabIndex = 19;
            // 
            // rtL_LookUpEdit1
            // 
            this.rtL_LookUpEdit1.Location = new System.Drawing.Point(237, 74);
            this.rtL_LookUpEdit1.Name = "rtL_LookUpEdit1";
            this.rtL_LookUpEdit1.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.rtL_LookUpEdit1.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.rtL_LookUpEdit1.Size = new System.Drawing.Size(210, 20);
            this.rtL_LookUpEdit1.TabIndex = 18;
            // 
            // rtL_ListBoxControl1
            // 
            this.rtL_ListBoxControl1.Location = new System.Drawing.Point(21, 613);
            this.rtL_ListBoxControl1.Name = "rtL_ListBoxControl1";
            this.rtL_ListBoxControl1.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.rtL_ListBoxControl1.Size = new System.Drawing.Size(210, 88);
            this.rtL_ListBoxControl1.TabIndex = 17;
            // 
            // rtL_LabelControl1
            // 
            this.rtL_LabelControl1.Location = new System.Drawing.Point(21, 594);
            this.rtL_LabelControl1.Name = "rtL_LabelControl1";
            this.rtL_LabelControl1.Size = new System.Drawing.Size(85, 13);
            this.rtL_LabelControl1.TabIndex = 16;
            this.rtL_LabelControl1.Text = "rtL_LabelControl1";
            // 
            // rtL_ImageEdit1
            // 
            this.rtL_ImageEdit1.Location = new System.Drawing.Point(21, 568);
            this.rtL_ImageEdit1.Name = "rtL_ImageEdit1";
            this.rtL_ImageEdit1.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.rtL_ImageEdit1.Size = new System.Drawing.Size(210, 20);
            this.rtL_ImageEdit1.TabIndex = 15;
            // 
            // rtL_ImageComboBoxEdit1
            // 
            this.rtL_ImageComboBoxEdit1.Location = new System.Drawing.Point(21, 542);
            this.rtL_ImageComboBoxEdit1.Name = "rtL_ImageComboBoxEdit1";
            this.rtL_ImageComboBoxEdit1.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.rtL_ImageComboBoxEdit1.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.rtL_ImageComboBoxEdit1.Size = new System.Drawing.Size(210, 20);
            this.rtL_ImageComboBoxEdit1.TabIndex = 14;
            // 
            // rtL_HyperLinkEdit1
            // 
            this.rtL_HyperLinkEdit1.EditValue = "i am hyperlink 1";
            this.rtL_HyperLinkEdit1.Location = new System.Drawing.Point(21, 516);
            this.rtL_HyperLinkEdit1.Name = "rtL_HyperLinkEdit1";
            this.rtL_HyperLinkEdit1.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.rtL_HyperLinkEdit1.Size = new System.Drawing.Size(210, 20);
            this.rtL_HyperLinkEdit1.TabIndex = 13;
            // 
            // rtL_GroupControl1
            // 
            this.rtL_GroupControl1.Controls.Add(this.rtL_ComboBoxEdit2);
            this.rtL_GroupControl1.Controls.Add(this.rtL_CheckEdit2);
            this.rtL_GroupControl1.Location = new System.Drawing.Point(21, 422);
            this.rtL_GroupControl1.Name = "rtL_GroupControl1";
            this.rtL_GroupControl1.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.rtL_GroupControl1.Size = new System.Drawing.Size(210, 88);
            this.rtL_GroupControl1.TabIndex = 12;
            this.rtL_GroupControl1.Text = "rtL_GroupControl1";
            // 
            // rtL_ComboBoxEdit2
            // 
            this.rtL_ComboBoxEdit2.Location = new System.Drawing.Point(7, 25);
            this.rtL_ComboBoxEdit2.Name = "rtL_ComboBoxEdit2";
            this.rtL_ComboBoxEdit2.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.rtL_ComboBoxEdit2.Size = new System.Drawing.Size(98, 20);
            this.rtL_ComboBoxEdit2.TabIndex = 13;
            // 
            // rtL_CheckEdit2
            // 
            this.rtL_CheckEdit2.Location = new System.Drawing.Point(5, 51);
            this.rtL_CheckEdit2.Name = "rtL_CheckEdit2";
            this.rtL_CheckEdit2.Properties.Caption = "rtL_CheckEdit2";
            this.rtL_CheckEdit2.Size = new System.Drawing.Size(100, 19);
            this.rtL_CheckEdit2.TabIndex = 13;
            // 
            // rtL_FontEdit1
            // 
            this.rtL_FontEdit1.Location = new System.Drawing.Point(21, 396);
            this.rtL_FontEdit1.Name = "rtL_FontEdit1";
            this.rtL_FontEdit1.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.rtL_FontEdit1.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.rtL_FontEdit1.Size = new System.Drawing.Size(210, 20);
            this.rtL_FontEdit1.TabIndex = 11;
            // 
            // rtL_FilterControl1
            // 
            this.rtL_FilterControl1.Cursor = System.Windows.Forms.Cursors.Arrow;
            this.rtL_FilterControl1.Location = new System.Drawing.Point(21, 288);
            this.rtL_FilterControl1.Name = "rtL_FilterControl1";
            this.rtL_FilterControl1.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.rtL_FilterControl1.Size = new System.Drawing.Size(210, 102);
            this.rtL_FilterControl1.TabIndex = 10;
            this.rtL_FilterControl1.Text = "rtL_FilterControl1";
            // 
            // rtL_DropDownButton1
            // 
            this.rtL_DropDownButton1.Location = new System.Drawing.Point(21, 259);
            this.rtL_DropDownButton1.Name = "rtL_DropDownButton1";
            this.rtL_DropDownButton1.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.rtL_DropDownButton1.Size = new System.Drawing.Size(210, 20);
            this.rtL_DropDownButton1.TabIndex = 9;
            this.rtL_DropDownButton1.Text = "rtL_DropDownButton1";
            // 
            // rtL_DateEdit1
            // 
            this.rtL_DateEdit1.EditValue = null;
            this.rtL_DateEdit1.Location = new System.Drawing.Point(21, 233);
            this.rtL_DateEdit1.Name = "rtL_DateEdit1";
            this.rtL_DateEdit1.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.rtL_DateEdit1.Properties.VistaTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.rtL_DateEdit1.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.rtL_DateEdit1.Size = new System.Drawing.Size(210, 20);
            this.rtL_DateEdit1.TabIndex = 8;
            // 
            // rtL_DataNavigator1
            // 
            this.rtL_DataNavigator1.Location = new System.Drawing.Point(21, 203);
            this.rtL_DataNavigator1.Name = "rtL_DataNavigator1";
            this.rtL_DataNavigator1.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.rtL_DataNavigator1.Size = new System.Drawing.Size(210, 24);
            this.rtL_DataNavigator1.TabIndex = 7;
            this.rtL_DataNavigator1.Text = "rtL_DataNavigator1";
            // 
            // rtL_ComboBoxEdit1
            // 
            this.rtL_ComboBoxEdit1.Location = new System.Drawing.Point(21, 177);
            this.rtL_ComboBoxEdit1.Name = "rtL_ComboBoxEdit1";
            this.rtL_ComboBoxEdit1.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.rtL_ComboBoxEdit1.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.rtL_ComboBoxEdit1.Size = new System.Drawing.Size(210, 20);
            this.rtL_ComboBoxEdit1.TabIndex = 6;
            // 
            // rtL_ColorEdit1
            // 
            this.rtL_ColorEdit1.EditValue = System.Drawing.Color.Empty;
            this.rtL_ColorEdit1.Location = new System.Drawing.Point(21, 151);
            this.rtL_ColorEdit1.Name = "rtL_ColorEdit1";
            this.rtL_ColorEdit1.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.rtL_ColorEdit1.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.rtL_ColorEdit1.Size = new System.Drawing.Size(210, 20);
            this.rtL_ColorEdit1.TabIndex = 5;
            // 
            // rtL_CheckEdit1
            // 
            this.rtL_CheckEdit1.Location = new System.Drawing.Point(19, 126);
            this.rtL_CheckEdit1.Name = "rtL_CheckEdit1";
            this.rtL_CheckEdit1.Properties.Caption = "rtL_CheckEdit1";
            this.rtL_CheckEdit1.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.rtL_CheckEdit1.Size = new System.Drawing.Size(212, 19);
            this.rtL_CheckEdit1.TabIndex = 4;
            // 
            // rtL_CheckedComboBoxEdit1
            // 
            this.rtL_CheckedComboBoxEdit1.EditValue = "";
            this.rtL_CheckedComboBoxEdit1.Location = new System.Drawing.Point(21, 100);
            this.rtL_CheckedComboBoxEdit1.Name = "rtL_CheckedComboBoxEdit1";
            this.rtL_CheckedComboBoxEdit1.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.rtL_CheckedComboBoxEdit1.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.rtL_CheckedComboBoxEdit1.Size = new System.Drawing.Size(210, 20);
            this.rtL_CheckedComboBoxEdit1.TabIndex = 3;
            // 
            // rtL_CheckButton1
            // 
            this.rtL_CheckButton1.Location = new System.Drawing.Point(21, 74);
            this.rtL_CheckButton1.Name = "rtL_CheckButton1";
            this.rtL_CheckButton1.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.rtL_CheckButton1.Size = new System.Drawing.Size(210, 20);
            this.rtL_CheckButton1.TabIndex = 2;
            this.rtL_CheckButton1.Text = "rtL_CheckButton1";
            this.rtL_CheckButton1.CheckedChanged += new System.EventHandler(this.rtL_CheckButton1_CheckedChanged);
            // 
            // rtL_CalcEdit1
            // 
            this.rtL_CalcEdit1.Location = new System.Drawing.Point(21, 48);
            this.rtL_CalcEdit1.Name = "rtL_CalcEdit1";
            this.rtL_CalcEdit1.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.rtL_CalcEdit1.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.rtL_CalcEdit1.Size = new System.Drawing.Size(210, 20);
            this.rtL_CalcEdit1.TabIndex = 1;
            // 
            // rtL_ButtonEdit1
            // 
            this.rtL_ButtonEdit1.EditValue = "Button_Edit";
            this.rtL_ButtonEdit1.Location = new System.Drawing.Point(21, 22);
            this.rtL_ButtonEdit1.Name = "rtL_ButtonEdit1";
            this.rtL_ButtonEdit1.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.rtL_ButtonEdit1.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.rtL_ButtonEdit1.Size = new System.Drawing.Size(210, 20);
            this.rtL_ButtonEdit1.TabIndex = 0;
            // 
            // ribbonControl1
            // 
            this.ribbonControl1.ApplicationButtonText = null;
            // 
            // 
            // 
            this.ribbonControl1.ExpandCollapseItem.Id = 0;
            this.ribbonControl1.ExpandCollapseItem.Name = "";
            this.ribbonControl1.Items.AddRange(new DevExpress.XtraBars.BarItem[] {
            this.ribbonControl1.ExpandCollapseItem});
            this.ribbonControl1.Location = new System.Drawing.Point(0, 0);
            this.ribbonControl1.MaxItemId = 1;
            this.ribbonControl1.Name = "ribbonControl1";
            this.ribbonControl1.Pages.AddRange(new DevExpress.XtraBars.Ribbon.RibbonPage[] {
            this.ribbonPage1});
            this.ribbonControl1.Size = new System.Drawing.Size(1031, 145);
            // 
            // ribbonPage1
            // 
            this.ribbonPage1.Groups.AddRange(new DevExpress.XtraBars.Ribbon.RibbonPageGroup[] {
            this.ribbonPageGroup1});
            this.ribbonPage1.Name = "ribbonPage1";
            this.ribbonPage1.Text = "ribbonPage1";
            // 
            // ribbonPageGroup1
            // 
            this.ribbonPageGroup1.Name = "ribbonPageGroup1";
            this.ribbonPageGroup1.Text = "ribbonPageGroup1";
            // 
            // XtraForm1_EditorC
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1031, 736);
            this.Controls.Add(this.ribbonControl1);
            this.Controls.Add(this.rtL_PopupContainerControl1);
            this.Controls.Add(this.rtL_CheckedListBoxControl1);
            this.Controls.Add(this.rtL_VScrollBar1);
            this.Controls.Add(this.rtL_TrackBarControl1);
            this.Controls.Add(this.rtL_TimeEdit1);
            this.Controls.Add(this.rtL_TextEdit1);
            this.Controls.Add(this.rtL_SplitContainerControl1);
            this.Controls.Add(this.rtL_SpinEdit1);
            this.Controls.Add(this.rtL_SimpleButton1);
            this.Controls.Add(this.rtL_RangeTrackBarControl1);
            this.Controls.Add(this.rtL_RadioGroup1);
            this.Controls.Add(this.rtL_ProgressBarControl1);
            this.Controls.Add(this.rtL_PopupContainerEdit1);
            this.Controls.Add(this.rtL_PanelControl1);
            this.Controls.Add(this.rtL_MRUEdit1);
            this.Controls.Add(this.rtL_MemoExEdit1);
            this.Controls.Add(this.rtL_MemoEdit1);
            this.Controls.Add(this.rtL_MarqueeProgressBarControl1);
            this.Controls.Add(this.rtL_LookUpEdit1);
            this.Controls.Add(this.rtL_ListBoxControl1);
            this.Controls.Add(this.rtL_LabelControl1);
            this.Controls.Add(this.rtL_ImageEdit1);
            this.Controls.Add(this.rtL_ImageComboBoxEdit1);
            this.Controls.Add(this.rtL_HyperLinkEdit1);
            this.Controls.Add(this.rtL_GroupControl1);
            this.Controls.Add(this.rtL_FontEdit1);
            this.Controls.Add(this.rtL_FilterControl1);
            this.Controls.Add(this.rtL_DropDownButton1);
            this.Controls.Add(this.rtL_DateEdit1);
            this.Controls.Add(this.rtL_DataNavigator1);
            this.Controls.Add(this.rtL_ComboBoxEdit1);
            this.Controls.Add(this.rtL_ColorEdit1);
            this.Controls.Add(this.rtL_CheckEdit1);
            this.Controls.Add(this.rtL_CheckedComboBoxEdit1);
            this.Controls.Add(this.rtL_CheckButton1);
            this.Controls.Add(this.rtL_CalcEdit1);
            this.Controls.Add(this.rtL_ButtonEdit1);
            this.Name = "XtraForm1_EditorC";
            this.Text = "XtraForm1_EditorC";
            ((System.ComponentModel.ISupportInitialize)(this.rtL_PopupContainerControl1)).EndInit();
            this.rtL_PopupContainerControl1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.rtL_CheckEdit5.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.rtL_CheckEdit4.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.rtL_CheckedListBoxControl1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.rtL_TrackBarControl1.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.rtL_TrackBarControl1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.rtL_TimeEdit1.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.rtL_TextEdit1.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.rtL_SplitContainerControl1)).EndInit();
            this.rtL_SplitContainerControl1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.rtL_SpinEdit1.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.rtL_RangeTrackBarControl1.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.rtL_RangeTrackBarControl1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.rtL_RadioGroup1.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.rtL_ProgressBarControl1.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.rtL_PopupContainerEdit1.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.rtL_PanelControl1)).EndInit();
            this.rtL_PanelControl1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.rtL_CheckEdit3.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.rtL_ComboBoxEdit3.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.rtL_MRUEdit1.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.rtL_MemoExEdit1.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.rtL_MemoEdit1.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.rtL_MarqueeProgressBarControl1.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.rtL_LookUpEdit1.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.rtL_ListBoxControl1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.rtL_ImageEdit1.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.rtL_ImageComboBoxEdit1.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.rtL_HyperLinkEdit1.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.rtL_GroupControl1)).EndInit();
            this.rtL_GroupControl1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.rtL_ComboBoxEdit2.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.rtL_CheckEdit2.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.rtL_FontEdit1.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.rtL_DateEdit1.Properties.VistaTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.rtL_DateEdit1.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.rtL_ComboBoxEdit1.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.rtL_ColorEdit1.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.rtL_CheckEdit1.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.rtL_CheckedComboBoxEdit1.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.rtL_CalcEdit1.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.rtL_ButtonEdit1.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ribbonControl1)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private RTL_ButtonEdit rtL_ButtonEdit1;
        private RTL_CalcEdit rtL_CalcEdit1;
        private RTL_CheckButton rtL_CheckButton1;
        private RTL_CheckedComboBoxEdit rtL_CheckedComboBoxEdit1;
        private RTL_CheckEdit rtL_CheckEdit1;
        private RTL_ColorEdit rtL_ColorEdit1;
        private RTL_ComboBoxEdit rtL_ComboBoxEdit1;
        private RTL_DataNavigator rtL_DataNavigator1;
        private RTL_DateEdit rtL_DateEdit1;
        private RTL_DropDownButton rtL_DropDownButton1;
        private RTL_FilterControl rtL_FilterControl1;
        private RTL_FontEdit rtL_FontEdit1;
        private RTL_GroupControl rtL_GroupControl1;
        private RTL_CheckEdit rtL_CheckEdit2;
        private RTL_ComboBoxEdit rtL_ComboBoxEdit2;
        private RTL_HyperLinkEdit rtL_HyperLinkEdit1;
        private RTL_ImageComboBoxEdit rtL_ImageComboBoxEdit1;
        private RTL_ImageEdit rtL_ImageEdit1;
        private RTL_LabelControl rtL_LabelControl1;
        private RTL_ListBoxControl rtL_ListBoxControl1;
        private RTL_LookUpEdit rtL_LookUpEdit1;
        private RTL_MarqueeProgressBarControl rtL_MarqueeProgressBarControl1;
        private RTL_MemoEdit rtL_MemoEdit1;
        private RTL_MemoExEdit rtL_MemoExEdit1;
        private RTL_MRUEdit rtL_MRUEdit1;
        private RTL_PanelControl rtL_PanelControl1;
        private RTL_CheckEdit rtL_CheckEdit3;
        private RTL_ComboBoxEdit rtL_ComboBoxEdit3;
        private RTL_PopupContainerEdit rtL_PopupContainerEdit1;
        private RTL_ProgressBarControl rtL_ProgressBarControl1;
        private RTL_RadioGroup rtL_RadioGroup1;
        private RTL_RangeTrackBarControl rtL_RangeTrackBarControl1;
        private RTL_SimpleButton rtL_SimpleButton1;
        private RTL_SpinEdit rtL_SpinEdit1;
        private RTL_SplitContainerControl rtL_SplitContainerControl1;
        private RTL_TextEdit rtL_TextEdit1;
        private RTL_TimeEdit rtL_TimeEdit1;
        private RTL_TrackBarControl rtL_TrackBarControl1;
        private RTL_VScrollBar rtL_VScrollBar1;
        private RTL_CheckedListBoxControl rtL_CheckedListBoxControl1;
        private RTL_PopupContainerControl rtL_PopupContainerControl1;
        private RTL_CheckEdit rtL_CheckEdit5;
        private RTL_CheckEdit rtL_CheckEdit4;
        private DevExpress.XtraBars.Ribbon.RibbonControl ribbonControl1;
        private DevExpress.XtraBars.Ribbon.RibbonPage ribbonPage1;
        private DevExpress.XtraBars.Ribbon.RibbonPageGroup ribbonPageGroup1;
    }
}